<?php

if(isset($_FILES['uploaded_image'])) {
    if($_FILES['uploaded_image']['error'] == 0) {
        
        $name = $DB->real_escape_string($_FILES['uploaded_image']['name']);
        $tname = $DB->real_escape_string($_FILES['uploaded_image']['tmp_name']);
		$fileHandler = new FileHandler(FILETYPE_QUESTION);
		if (!$fileHandler->upload(file_get_contents($tname), $name, '')) {
            $message->addError("File {$name} and beyond are not uploaded. Please upload them again.");
            redirect('RUSER_UPLOAD');
        }
    }
    else {
        echo 'An error accured while the file was being uploaded. '
           . 'Error code: '. intval($_FILES['uploaded_image']['error']);
    } 
}
else {
    echo 'Error! A file was not sent!';
}
echo '<p>Click <a href="uploadpaper.php">here</a> to go back</p>';
?>
 
 
 
 