# python
import socket
import select
import time
from wand.image import Image

PORT = 10001
MAX_CLIENTS = 10
PACKET_SIZE = 2048

def recv_all(sock):
	'''Receive everything from `sock`, until timeout occurs, meaning sender
	is exhausted, return result as string.'''

	prev_timeout = sock.gettimeout()
	try:
		# Set Timeout to very low in case sock.recv hangs(for not getting enough data)
		sock.settimeout(0.01)

		rdata = []
		i = 0
		while True:
			try:
				data = sock.recv(PACKET_SIZE)
				if (data == ''):
					i = i + 1
				else:
					i = 0
				if (i == 10):
					return('', 1)
				rdata.append(data)
			except socket.timeout:
				if len(rdata) == 0:
					sock.settimeout(prev_timeout)
					return ('', 1)
				else:
					sock.settimeout(prev_timeout)
					return (''.join(rdata), 0)

		# unreachable
	finally:
		# Set back to previous timeout
		sock.settimeout(prev_timeout)

# create an INET, STREAMing socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Bind to public host
sock.bind(('', PORT))
# Listen upto 5 connection request at a time.
sock.listen(MAX_CLIENTS)

SOCKET_LIST = list()
SOCKET_LIST.append(sock)


print("Running at "+ socket.gethostname())

while 1:
	# Get Read Sockets
	# 5 SEconds Timeout
		# Accept Connection
		(client, address) = sock.accept()
		print "Client("+ address[0] +") Connected at "+ time.strftime('%a, %d %b %Y %H:%M:%S') +"."
		#client = csockfd
		(addr, port) = client.getpeername()
		addridx = str(addr) +":"+ str(port)
		# Get the Data (Returns 0 length if timeout)
		#(data, error) = recv_all(client)
		data = client.recv(PACKET_SIZE)
		error = 0
		# Some Error, Close the connection.
		if (error > 0 or len(data) == 0):
			client.close()
			continue
		# Normalize the Data
		data = ''.join((line + '\n') for line in data.splitlines())

		data = data.strip()
		print(data[:13], data[13:])
		if (data[:12] == 'TEMP_PROCESS'):
			print("Processing...")
			file_name = data[13:]
			im = Image(filename='uploads/tmp/'+ file_name, resolution=350)
			for i, page in enumerate(im.sequence):
				with Image(page) as page_image:
					dir_name = 'uploads/paper/'+file_name
					page_image.save(filename=dir_name)
					print("Saved("+ i +") to "+dir_name)


		# Send the Data
		client.send('DONE')
		# Check if to disconnect the client or not
		client.close()

sock.close()