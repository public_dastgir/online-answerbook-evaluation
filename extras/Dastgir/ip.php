<?php
$a = inet_pton("127.0.0.1");
$array = unpack("C4", $a);
var_dump($array);
echo "Size: ". strlen($a) ."\n";
if (defined('AF_INET6')) {
  echo "PHP was compiled without --disable-ipv6 option";
} else {
  echo "PHP was compiled with --disable-ipv6 option";
}


$string = 'FF ';
if (ctype_xdigit($string)) {
	echo "\nThe string $string consists of all hexadecimal digits.\n";
} else {
	echo "\nThe string $string does not consist of all hexadecimal digits.\n";
}
?>