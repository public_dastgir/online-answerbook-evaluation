<?php
if (isset($argv) && count($argv) == 2) {
	echo password_hash($argv[1], PASSWORD_BCRYPT);
} else {
	echo "Please run script as:\n";
	echo "php generatePassword.php PASSWORD_HERE";
}
?>