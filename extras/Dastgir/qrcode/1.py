# Required: pyqrcode, qrtools, pypng, wand
# pip install pyqrcode pypng
# sudo apt-get install python-qrtools
import pyqrcode
import qrtools
qr = pyqrcode.create('{"studId": 1, "qp_code": 10000}')
qr.png("qr_code.png", scale=6)

qr2 = qrtools.QR()
qr2.decode("qr_code.png")
print qr2.data
