# wand
from wand.image import Image
# Converting first page into JPG
'''
with Image(filename="img-205153844.pdf", resolution=200) as img:
	print(img)
'''
im = Image(filename='img-205153844.pdf', resolution=200)
for i, page in enumerate(im.sequence):
    with Image(page) as page_image:
        print(page_image)
        #page_image.save(filename='/tmp/foo.pdf.images/page-%s.png' % i)
    #img.save(filename="2.png")
'''
# Resizing this image
with Image(filename="/temp.jpg") as img:
     img.resize(200, 150)
     img.save(filename="/thumbnail_resize.jpg")
'''