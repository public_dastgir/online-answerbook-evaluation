# USAGE
# python test_grader.py --image images/test_01.png
# pip install scipy/numpy/imutils/opencv2-python
# http://www.pyimagesearch.com/2016/10/03/bubble-sheet-multiple-choice-scanner-and-test-grader-using-omr-python-and-opencv/

# import the necessary packages
from imutils.perspective import four_point_transform
from imutils import contours
import numpy as np
import argparse
import imutils
import cv2

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
	help="path to the input image")
args = vars(ap.parse_args())

# define the answer key which maps the question number
# to the correct answer
# ANSWER_KEY = {0: 1, 1: 4, 2: 0, 3: 3, 4: 1}

# load the image, convert it to grayscale, blur it
# slightly, then find edges
image = cv2.imread(args["image"])

for omr_i in range(0, 2):
	if (omr_i == 1):	# Check if 2nd OMR is filled.
		omrs = image[0:1600,0:250]
	else:
		omrs = image[1601:,0:250]
	#cv2.imshow("cropped", image)
	#cv2.waitKey(0)
	#

	gray = cv2.cvtColor(omrs, cv2.COLOR_BGR2GRAY)
	blurred = cv2.GaussianBlur(gray, (5, 5), 0)
	edged = cv2.Canny(blurred, 75, 200)
	'''
	# find contours in the edge map, then initialize
	# the contour that corresponds to the document
	cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
	cnts = cnts[0] if imutils.is_cv2() else cnts[1]
	docCnt = None

	# ensure that at least one contour was found
	if len(cnts) > 0:
		# sort the contours according to their size in
		# descending order
		cnts = sorted(cnts, key=cv2.contourArea, reverse=True)

		# loop over the sorted contours
		for c in cnts:
			# approximate the contour
			peri = cv2.arcLength(c, True)
			approx = cv2.approxPolyDP(c, 0.02 * peri, True)

			# if our approximated contour has four points,
			# then we can assume we have found the paper
			if len(approx) == 4:
				docCnt = approx
				break
	# apply a four point perspective transform to both the
	# original image and grayscale image to obtain a top-down
	# birds eye view of the paper
	paper = four_point_transform(image, docCnt.reshape(4, 2))
	warped = four_point_transform(gray, docCnt.reshape(4, 2))
	'''
	paper = omrs
	warped = gray

	# apply Otsu's thresholding method to binarize the warped
	# piece of paper
	thresh = cv2.threshold(warped, 0, 255,
		cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

	# find contours in the thresholded image, then initialize
	# the list of contours that correspond to questions
	cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
	cnts = cnts[0] if imutils.is_cv2() else cnts[1]
	questionCnts = []

	# loop over the contours
	for c in cnts:
		# compute the bounding box of the contour, then use the
		# bounding box to derive the aspect ratio
		(x, y, w, h) = cv2.boundingRect(c)
		ar = w / float(h)

		# in order to label the contour as a question, region
		# should be sufficiently wide, sufficiently tall, and
		# have an aspect ratio approximately equal to 1
		if (w >= 45):
			print(x,y,w,h,ar,len(questionCnts))
		# H = 20, W = 20
		# W = 12 H = 12 ar = 0.9-1.3
		if w >= 40 and h >= 40 and ar >= 0.8 and ar <= 1.3:
			print(x,y,w,h)
			questionCnts.append(c)

	# sort the question contours top-to-bottom, then initialize
	# the total number of correct answers
	questionCnts = contours.sort_contours(questionCnts, method="top-to-bottom")[0]
	#questionCnts = contours.sort_contours(questionCnts, method="left-to-right")[0]
	correct = 0

	main_question_number = -1
	sub_questions = list()
	if (len(questionCnts)/2 != 9):
		# 9 Questions not scanned.
		continue
	# each question has 5 possible answers, to loop over the
	# question in batches of 5
	for (q, i) in enumerate(np.arange(0, len(questionCnts), 2)):
		# sort the contours for the current question from
		# left to right, then initialize the index of the
		# bubbled answer
		cnts = contours.sort_contours(questionCnts[i:i + 2])[0]
		bubbled = None
		bubble_number = 0
		# loop over the sorted contours
		for (j, c) in enumerate(cnts):
			bubble_number = bubble_number + 1
			# construct a mask that reveals only the current
			# "bubble" for the question
			mask = np.zeros(thresh.shape, dtype="uint8")
			cv2.drawContours(mask, [c], -1, 255, -1)

			# apply the mask to the thresholded image, then
			# count the number of non-zero pixels in the
			# bubble area
			mask = cv2.bitwise_and(thresh, thresh, mask=mask)
			total = cv2.countNonZero(mask)

			# if the current total has a larger number of total
			# non-zero pixels, then we are examining the currently
			# bubbled-in answer
			#if bubbled is None or total > bubbled[0]:
			print(j)
			if total > 2000:
				'''
				if (bubbled is not None):
					print(total, bubbled[0], "Hi")
				else:
					print(total, "Hi")
				'''
				if (bubble_number == 1):
					if (main_question_number != -1):
						# Multiple Marked check other omr.
						pass
					else:
						main_question_number = q
				else:
					print("Number "+ str(q))
					sub_questions.append(q)
				#print("Question "+ str(q) +": Bubbled:"+ str(j))
			cv2.drawContours(paper, [cnts[j]], -1, (255, 0, 255), 3)
			print("Question"+ str(q))

		print(main_question_number, sub_questions)
		# initialize the contour color and the index of the
		# *correct* answer
	if (omr_i == 0 and main_question_number != -1):
		break

	# grab the test taker
	'''
	score = (correct / 5.0) * 100
	print("[INFO] score: {:.2f}%".format(score))
	cv2.putText(paper, "{:.2f}%".format(score), (10, 30),
		cv2.FONT_HERSHEY_SIMPLEX, 0.9, (0, 0, 255), 2)
	#cv2.imshow("Original", image)
	#cv2.imshow("Exam", paper)
	'''
	cv2.imwrite("exam.jpg", paper)

	cv2.waitKey(0)