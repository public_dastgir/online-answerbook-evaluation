<html>
<style>
<script type="text/javascript" language="JavaScript">
function checkPassword(theForm) {
    if (theForm.New_Password.value != theForm.Repeat_Password.value)
    {
        alert('These passwords don\'t match!. Please retype the password');
        return false;
    } else {
        return true;
    }
}
</script>
<script>
form {
    border: 3px solid #f1f1f1;
}

input[type=text], input[type=password] {
    width: 100%;
    padding: 12px 14px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

button {
    background-color: #4CAF50;
    color: white;
	align: center;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

button:hover {
    opacity: 0.8;
}

.confirmbtn {
    width: auto;
    padding: 15px 70px;
    background-color: #00cc00;
}
.container {
    padding: 16px;
}

</style>
<body>
<div align="center">

<h2><i><b>Update Profile</i></b></h2>
</div>

<form action="Success.php" method="post">

  <div class="container">
     <label><b><h3><i>Teachername</i></b></h3></label>
    <input type="text" placeholder="Enter Teachername" name="tname" required>

    <label><i><h3><b>Username</b></i></h3></label>
    <input type="text" placeholder="Enter Username" name="uname" required>
	
	<form action="../" onsubmit="return checkPassword(this);">
	<label><i><b><h3>New Password</b></i></h3></label>
    <input type="password" placeholder="Enter New Password" name="npsw" required>
    
	<label><i><h3><b>Repeat Password</b></i></h3></label>
    <input type="password" placeholder="Repeat Password" name="rpsw" required>
	</form>
  </div>

  <div align="center">
    <button type="submit" class="confirmbtn">Confirm</button>
  </div>
</form> 

</body>
</html>