<html>
<head>
<title>Change Teacher Subject</title>
</head>
<style>
form {
    border: 3px solid #f1f1f1;
}

input[type=text]{
    width: 100%;
    padding: 12px 14px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}
button {
    background-color: #4CAF50;
    color: white;
	align: center;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

button:hover {
    opacity: 0.8;
}

.alert {
    padding: 20px;
    background-color: #f44336; /* Red */
    color: white;
    margin-bottom: 15px;
}

.closebtn {
    margin-left: 15px;
    color: white;
    font-weight: bold;
    float: right;
    font-size: 22px;
    line-height: 20px;
    cursor: pointer;
    transition: 0.3s;
}

.closebtn:hover {
    color: black;
}

.confirmbtn {
    width: auto;
    padding: 15px 70px;
    background-color: #00cc00;
}
.container {
    padding: 16px;
}
</style>
<body>
<div align="center">

<h2><i><b>Changing Subject of Teacher</i></b></h2>
</div>

<form action="Success.php" method="post" onsubmit="return checkPas(this);">

  <div class="container">
    
	<label><b><h3><i>Change Teachers Subject</i></b></h3></label>
    <input type="text" placeholder="Enter the Teachers Subject you want to change" name="tsub" required>
	
   <div align="center">
    <button type="submit" class="confirmbtn">Confirm</button>
	<div class="alert">
  	<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
  	<strong>Are you sure you want to proceed</strong>
	</div>
   </div>
 </form> 
</body>
</html>