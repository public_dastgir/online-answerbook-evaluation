<html>
<head>
<title> Update Teacher Information
</title>
</head>
<style>
.container {
    padding: 16px;
}
button {
    background-color: #4CAF50;
    color: white;
	align: center;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: auto;
}
.chbtn {
    width: auto;
    padding: 15px 70px;
    background-color: #3399ff;
}
button:hover {
    opacity: 0.8;
}
</style>
<!-- 
<script type="text/javascript" language="JavaScript">
function checkPas(theForm) {
    if (theForm.nepsw.value != theForm.repsw.value)
    {
        alert('These Password don\'t match!.');
        return false;
    } else {
        return true;
    }
}
</script>
-->
<body>

<form action="conf.php" method="post">
<div class="container">
    
	<label><b><h3><i>User name</i></b></h3></label>
    <input type="text" placeholder="Enter Username of the teacher" name="uname" required>

    <label><i><h3><b>College_ID</b></i></h3></label>
    <input type="text" placeholder="Enter the College_ID of the teacher" name="cid" required>
	
	<div align="center">
    <button type="submit" class="chbtn">Confirm</button>
   </div>

</div>
</form>
</body>
</html>