<?php

/**
 * Contains Informatin about Constant Name and Values.
 * @var array
 */
$constants = [
				/**
				 * Constans for lib/Message.php
				 */
				['MESSAGE_DEBUG', 1],
				['MESSAGE_ERROR', 2],
				['MESSAGE_INFO' , 4],
			];

/** Defines Constants */
for ($i = 0; $i < count($constants); $i++) {
	if (defined($constants[$i][0])) {
		throw new Exception("Constant {$constants[$i][0]} is Already defined.");
	}
	define($constants[$i][0], $constants[$i][1]);
}
?>
