<?php
/**
 * Contains useful information for redirects.
 * Format:
 * 	'RedirectUniqueConstant' => ['RedirectUrl(FileName)', 'role_name', 'Title of Page(Blank for Default)'],
 * @var Array
 */
$redirect_indexes = [
						/** User Redirects */
						'RUSER_INDEX'              => ['login/'                       , ''                  , ''         ],
						'RUSER_PROFILE'            => ['profile/home/'                , 'view_profile'      , 'Home Page'],
						'RUSER_PROFILE_V'          => ['profile/view/'                , 'view_profile'      , 'View Profile'],
						'RUSER_PROFILE_U'          => ['profile/update/'              , 'update_profile'    , 'Update Profile'],
						'RUSER_ALLOC'			   => ['displayalloc/'			  	  , 'allocate'			, 'Allocate'],
						'RUSER_ADD_FACULTY'        => ['profile/add_faculty/'         , 'add_faculty'       , 'Add Faculty'],
						'RUSER_UPLOAD'             => ['upload/upload/'               , 'upload_papers'		, 'Upload'],
						'RUSER_UPLOAD_VIEW'        => ['upload/list_files/'           , 'upload_papers'		, 'List'],
						'RUSER_VALIDATE_PAPER'     => ['paper/validate/'              , 'validate_paper'    , 'Validate Paper'],
						'RUSER_STUDENT'			   => ['student/add_student/'		  , 'add_student'		, 'Add Student'],
						'RUSER_STUDENT_DB'		   => ['student/add_db/'			  , 'add_student'		, ''],
						'RUSER_ADD_COLLEGE'        => ['college/add/'                 , 'add_college'       , 'Add College'],
						'RUSER_ADD_SUBJECT'        => ['subject/add/'                 , 'add_subject'       , 'Add Subject'],
						'RUSER_SUBJECT'			   => ['subject/'					  , 'assign'			, 'Assign Subject'],
						'RUSER_ADD_QP'             => ['qp_code/'                     , 'add_qp_code'       , 'Add QPCode'],
						'RUSER_SYS1'			   => ['get_status/'				  , 'teacher_list'		, 'List of Teacher'],
						'RUSER_SYS2'			   => ['cp/'						  , 'system_info'		, 'System Info'],
						'RUSER_ADD_QUESTIONS'      => ['paper/question/'              , 'add_question'      , 'Add Question'],

						/** System Redirects */
						'RSYSTEM_LOGIN'            => ['system/login/'                , ''                  , ''         ],
						'RSYSTEM_LOGOUT'           => ['system/logout/'               , 'view_profile'      , ''         ],

						'RSYSTEM_UPDATE_PROFILE'   => ['system/profile/update/'       , 'update_profile'    , ''         ],
						'RSYSTEM_ADD_FACULTY'      => ['system/profile/add_faculty/'  , 'add_faculty'       , ''         ],
						'RSYSTEM_ALLOC'			   => ['system/displayalloc/'		  , 'allocate'			,''			 ],
						'RSYSTEM_DEALLOC'		   => ['system/dealloc/'			  , 'deallocate'		, ''		 ],
						'RSYSTEM_ADD_COLLEGE'      => ['system/college/add/'          , 'add_college'       , ''         ],
						'RSYSTEM_UPLOAD_FILE'      => ['system/upload/add_file/'      , 'upload_papers'     , 'Upload'],
						'RSYSTEM_ADD_QP'		   => ['system/qp_code/'			  , 'add_qp_code'		,''],
						'RSYSTEM_ADD_SUBJECT'      => ['system/subject/add/'          , 'add_subject'       , ''],
						'RSYSTEM_SUBJECT'		   => ['system/subject/'			  , 'assign'			, ''],
						'RSYSTEM_GET_QUESTION'     => ['system/getQuestion/'          , 'validate_paper'    , ''],
						'RSYSTEM_ADD_QUESTIONS'    => ['system/paper/question/'       , 'add_question'      , ''],
						'RSYSTEM_GET_QUESTIONS'    => ['system/paper/getAllQuestions/', 'validate_paper'    , ''],
						'RSYSTEM_ADD_MARKS'        => ['system/paper/addMarks/'       , 'validate_paper'    , ''],
						'RSYSTEM_COMPLETE_PAPER'   => ['system/paper/complete/'       , 'validate_paper'    , ''],
						/** CSS Redirects */
						'RCSS_LOGIN'               => ['css/login/'                   , ''                  , ''         ],
						'RCSS_UPLOAD'              => ['css/upload/'                  , ''                  , ''         ],
						'RCSS_MATERIALIZE'         => ['css/materialize/'             , 'view_profile'      , ''         ],
						'RCSS_MATERIALIZE_MIN'     => ['css/materialize.min/'         , 'view_profile'      , ''         ],
						'RCSS_MATERIAL_ICONS'      => ['css/material_icons/'          , 'view_profile'      , ''         ],
						'RCSS_MORRIS'              => ['css/morris/'                   , 'view_profile'      , ''         ],

						/** JS Redirect */
						'RJS_JQUERY'               => ['js/jquery.min/'               , 'view_profile'      , ''         ],
						'RJS_NAVBAR'               => ['js/navbar/'                   , 'view_profile'      , ''         ],
						'RJS_MATERIALIZE'          => ['js/materialize/'              , 'view_profile'      , ''         ],
						'RJS_MATERIALIZE_MIN'      => ['js/materialize.min/'          , 'view_profile'      , ''         ],
						'RJS_UPLOAD'               => ['js/upload/'                   , 'upload_paper'      , ''         ],
						'RJS_PAPER_V'              => ['js/validate_paper/'           , 'validate_paper'    , ''         ],
						'RJS_MORRIS'               => ['js/morris.min/'               , 'view_profile'      , ''         ],
						'RJS_RAPHEAL'              => ['js/raphael.min/'              , 'view_profile'      , ''         ],
					];
/**
 * Request Methods for Specific Redirect Indexes
 * Format:
 *  'RedirectUniqueConstant' => 'RequestType'
 * IF not mentioned here, it will be assumed as GET
 * @var array
 */
$request_methods = [
					'RSYSTEM_LOGIN' => 'POST',
					'RSYSTEM_UPDATE_PROFILE' => 'POST',
					'RSYSTEM_ADD_FACULTY' => 'POST',
					'RSYSTEM_ADD_COLLEGE' => 'POST',
					'RSYSTEM_UPLOAD_FILE' => 'POST',
					'RSYSTEM_ADD_SUBJECT' => 'POST',
					'RUSER_STUDENT_DB' => 'POST',
					'RSYSTEM_ALLOC' => 'POST',
					'RSYSTEM_DEALLOC' => 'POST',
					'RSYSTEM_SUBJECT' => 'POST',
					'RSYSTEM_ADD_QP' => 'POST',
					'RSYSTEM_GET_QUESTION' => 'POST',
					'RSYSTEM_ADD_QUESTIONS' => 'POST',
					'RSYSTEM_GET_QUESTIONS' => 'POST',
					'RSYSTEM_ADD_MARKS' => 'POST',
];
?>
