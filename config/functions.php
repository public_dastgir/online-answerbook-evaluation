<?php
/**
 * Autoload Module if not already loaded.
 * Checks for module first, then check for trait.
 * @method __autoload
 * @param  string	 $classname ClassName
 */
function __autoload($classname)
{
	$filename = SIH_PATH . '/lib/'. $classname . '.php';
	if (file_exists($filename) == NULL) {
		debug_print_backtrace();
		throw new Exception("Cannot find $classname.");
	}
	require_once($filename);
}

/**
 * Returns the ClientIP
 * @method getClientIP
 * @return string	  IP Address(IPv4/IPv6)
 */
function getClientIP()
{
	$ipaddress = '';
	if (isset($_SERVER['HTTP_CLIENT_IP'])) {
		$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	} else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
		$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	} else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
		$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	} else if (isset($_SERVER['HTTP_FORWARDED'])) {
		$ipaddress = $_SERVER['HTTP_FORWARDED'];
	} else if (isset($_SERVER['REMOTE_ADDR'])) {
		$ipaddress = $_SERVER['REMOTE_ADDR'];
	} else {
		throw new Exception("Error(Code: PI), Please Contact Site Administrator(". SITE_OWNER_EMAIL .")");
	}
 
	return $ipaddress;
}

/**
 * Gives RedirectUrl from constant
 * @method getRedirectUrl
 * @param  string         $redirectUniqueString Redirect Constant
 * @return string                               Fully Qualified URL
 */
function getRedirectUrl($redirectUniqueString)
{
	if (!defined($redirectUniqueString)) {
		throw new Exception("getRedirectUrl: Invalid Unique Constant: {$redirectUniqueString}.");
	}
	return SIH_DIR_NAME.constant($redirectUniqueString);
}

/**
 * Registers Redirect Constants from $redirect_indexes
 * Also Registers the page as GET method only if not given.
 * @method registerRedirectConstants
 */
function registerRedirectConstants()
{
	global $redirect_indexes, $request_methods;
	foreach ($redirect_indexes as $key => $redirect_to) {
		if (defined($key)) {
			throw new Exception("{$key} Constant is already defined.");
		}
		// If does not exist in request_methods, default to GET.
		if (!isset($request_methods[$key])) {
			$request_methods[$key] = 'GET';
		}
		define($key, $redirect_to[0]);
	}
}

/**
 * Redirects the User to provided Url
 * @method redirect
 * @param  string   $redirectUniqueString Redirect Constant
 */
function redirect($redirectUniqueString)
{
	global $DB;
	// Close DB before redirect
	$DB->close();
	header("Location: ". getRedirectUrl($redirectUniqueString));
	die();
}

/**
 * Checks if arguments in POST request are set and not empty
 * @method checkPost
 * @param  array    $arguments  contains name to be checked
 * @return bool                 true, if no error.
 */
function checkPost($arguments)
{
	for ($i = 0; $i < count($arguments); $i++) {
		if (!isset($_POST[$arguments[$i]]) || empty($_POST[$arguments[$i]])) {
			return false;
		}
	}
	return true;
}
?>
