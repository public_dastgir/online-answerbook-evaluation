<?php
/**
 * Path to Root Directory
 */
define('SIH_PATH', dirname(__FILE__)."/../");
/**
 * Folder inside directory
 * e.g: http://localhost/SIH2k17/, so SIH_DIR_NAME will be '/SIH2k17/'
 */
define('SIH_DIR_NAME', '/SIH2k17/');

/**
 * AutoUpdate SQL?
 */
define('SQL_AUTO_UPDATE', true);

/**
 * Email Address of Site Owner
 */
define('SITE_OWNER_EMAIL', 'dastgirp@gmail.com');

/**
 * Is System 32 Bit?
 */
define('IS_32_BIT', (PHP_INT_SIZE <= 4)?TRUE:FALSE);

/**
 * Web Servce IP
 */
define('WEB_SERVICE_IP', '127.0.0.1');
define('WEB_SERVICE_PORT', 10001);
?>
