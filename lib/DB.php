<?php
/**
 * DB Class
 */

class DB extends mysqli
{
	/**
	 * MySQLi Constructor
	 * @method __construct
	 */
	function __construct()
	{
		parent::__construct(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
		if ($this->connect_errno > 0) {
            die('Connect Error (' . $this->connect_errno .'): '. $this->connect_error);
        }
	}

	/**
	 * Gives the ErrorNumber with Error Message.
	 * Useful if someone just wants to trim the connect_error
	 * @method generateErrorMessage
	 * @return string               Error Message
	 */
	public function generateErrorMessage()
	{
		return('(' . $this->errno . ') '. $this->error);
	}

	/**
	 * Returns the List of Colleges
	 * @method getColleges
	 * @return array        College List
	 */
	public function getColleges($collegeId = 0)
	{
		$collegeId = intval($collegeId);
		if ($collegeId == 0) {
			$result = $this->query("SELECT * FROM `college` ORDER BY `id`");
			if ($result && $result->num_rows > 0) {
				$colleges = [];
				while ($row = $result->fetch_assoc()) {
					$colleges[$row['id']] = $row['name'];
				}
			}
			return $colleges;
		} else {
			$result = $this->query("SELECT `name` FROM `college` WHERE `id`='$collegeId'");
			if ($result && $result->num_rows > 0) {
				$row = $result->fetch_assoc();
				return $row['name'];
			}
			throw new Exception("Invalid College");
		}
	}
}
?>
