<?php
/**
 * User Class (Variables)
 * Should only contain variables that are required to be saved in sessions.
 */

class User
{
	/**
	 * User ID
	 * @var int
	 */
	public $id = 0;
	/**
	 * Role of User
	 * @see /lib/Role.php
	 * @var int
	 */
	public $role = 0;
	/**
	 * Name of User
	 * @var string
	 */
	public $name = "Unknown";
	/**
	 * IP from which user is logged in.
	 * @var string
	 */
	public $ip = "0.0.0.0";
	/**
	 * Is User IP Locked?
	 * @var boolean
	 */
	public $ip_locked = true;
	/**
	 * Sex of User
	 * Valid Values:
	 * M, F, O
	 * @var enum
	 */
	public $sex = 'M';
	/**
	 * College ID
	 * @var int
	 */
	public $college_id = -1;
	/**
	 * Mobile Number of User
	 * @var string
	 */
	public $mobile_number = '0000000000';
	/**
	 * Email of User
	 * @var String
	 */
	public $email = 'a@a.com';
	/**
	 * Subjects assigned
	 * @var array
	 */
	public $subjects = [];
	public $subject_names = [];
}
?>
