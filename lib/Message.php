<?php 

/**
 * Message Handler
 * This class can add Messages such as Information/Debug/Error into Session for furthur use.
 */

class Message
{
	private $debug = [];    //< Type = 1
	private $error = [];    //< Type = 2
	private $info = [];     //< Type = 4

	/**
	 * Constructor for MessageHandler
	 * Reinitializes the variables from Session.
	 * @method __construct
	 */
	function __construct()
	{
		if (isset($_SESSION['message'])) {
			// Set the Variables
			if (isset($_SESSION['message']['debug']) && !empty($_SESSION['message']['debug'])) {
				$this->debug = $_SESSION['message']['debug'];
			} else if (isset($_SESSION['message']['error']) && !empty($_SESSION['message']['error'])) {
				$this->error = $_SESSION['message']['error'];
			} else if (isset($_SESSION['message']['info']) && !empty($_SESSION['message']['info'])) {
				$this->info = $_SESSION['message']['info'];
			}
		}
	}

	/**
	 * Saves all the local Variables into Session
	 * @method saveAll
	 * @param  int     $type Message Constants(see constants.php)
	 */
	private function saveAll($type)
	{
		/// Checks if Session have message, else reinitialize
		if (!isset($_SESSION['message'])) {
			$_SESSION['message'] = [];
		}
		// Destroy the Array and save into Session
		if ($type & MESSAGE_DEBUG) {
			$_SESSION['message']['debug'] = $this->debug;
		} else if ($type & MESSAGE_ERROR) {
			$_SESSION['message']['error'] = $this->error;
		} else if ($type & MESSAGE_INFO) {
			$_SESSION['message']['info'] = $this->info;
		}	
	}

	/** Clear Functions */
	/**
	 * Clears All the Local variable as well as Session.
	 * @method clear
	 * @param  int      $type Message Constants(see constants.php)
	 */
	private function clear($type)
	{
		// Destroy the Array and save into Session
		if ($type & MESSAGE_DEBUG) {
			unset($this->debug);
			$this->debug = [];
		} else if ($type & MESSAGE_ERROR) {
			unset($this->error);
			$this->error = [];
		} else if ($type & MESSAGE_INFO) {
			unset($this->info);
			$this->info = [];
		}
		// Save the changed information into Session
		$this->saveAll($type);
	}

	/**
	 * Quick Function to clear all messages
	 * @method clearAll
	 */
	public function clearAll()
	{
		$this->clear(MESSAGE_DEBUG | MESSAGE_ERROR | MESSAGE_INFO);
	}

	/**
	 * Clears only Debug Message
	 * @method clearDebug
	 */
	public function clearDebug()
	{
		$this->clear(MESSAGE_DEBUG);
	}

	/**
	 * Clears only Error Message
	 * @method clearError
	 */
	public function clearError()
	{
		$this->clear(MESSAGE_ERROR);
	}

	/**
	 * Clears only Info Message
	 * @method clearInfo
	 */
	public function clearInfo()
	{
		$this->clear(MESSAGE_INFO);
	}

	/** Add Functions */

	/**
	 * Adds Message into Debug variable as well as Session
	 * @method addDebug
	 * @param  string   $message Message to store
	 */
	public function addDebug($message)
	{
		$this->debug[] = $message;
		$this->saveAll(MESSAGE_DEBUG);
	}

	/**
	 * Adds Message into Error variable as well as Session
	 * @method addError
	 * @param  string   $message Message to store
	 */
	public function addError($message)
	{
		$this->error[] = $message;
		$this->saveAll(MESSAGE_ERROR);
	}

	/**
	 * Adds Message into Info variable as well as Session
	 * @method addInfo
	 * @param  string   $message Message to store
	 */
	public function addInfo($message)
	{
		$this->info[] = $message;
		$this->saveAll(MESSAGE_INFO);
	}

	/** Retrieving Messages */
	/**
	 * Gets all the debug message
	 * @method getDebug
	 * @return array   Debug Messages Array
	 */
	public function getDebug()
	{
		return $this->debug;
	}

	/**
	 * Gets all the error message
	 * @method getError
	 * @return array   Error Messsages Array
	 */
	public function getError()
	{
		return $this->error;
	}

	/**
	 * Gets all the information message
	 * @method getInfo
	 * @return array  Information Messages Array
	 */
	public function getInfo()
	{
		return $this->info;
	}
}
?>
