<?php

define('ROLE_COLLEGE_T', 1);
define('ROLE_COLLEGE_A', 2);
define('ROLE_UNI_S', 4);
define('ROLE_UNI_A', 8);

class Role {
	/** @var array Array Containing TitleID and Title Name */
	public $titles = [
								1, 'College Teacher',
								2, 'College Admin',
								4, 'University Staff',
								8, 'University Admin',
	];

	/**
	 * Array containing All the roles with assigned titles.
	 * Roles are inherited by later title's.
	 * Format:
	 * TitleId => array('ValidRoles',...)
	 * @var array
	 */
	public $roles = [
		/** College Teacher */
		1 => [
				'view_profile',
				'update_profile',
				'allocate',
				'deallocate',
				'validate_paper',
			],
		/** College Admin */
		2 => [
				'view_profile',
				'update_profile',
				'add_student',
				'add_faculty',
				'assign',
				'teacher_list',
			],
		/** University Staff */
		4 => [
				'view_profile',
				'update_profile',
				'upload_papers',
				'add_subject',
				'add_qp_code',
			],
		/** University Admin */
		8 => [
				'view_profile',
				'update_profile',
				'add_faculty',
				'add_college',
				'system_info',
				'add_question',
			],
	];
}
?>