<?php
/**
 * Navigation Class for Generating NavBar
 */

class Navigation
{
	/**
	 * NavBar Elements.
	 * Format:
	 * [
	 * 	['Name of Element', 'Link'],
	 * 	[],
	 * 	...
	 * ]
	 * 'Link' can be converted to array, to support dropdowns
	 * @var array
	 */
	private $elements = [
							['Home', 'RUSER_PROFILE'],
							['Profile', [
											['View', 'RUSER_PROFILE_V'],
											['Update', 'RUSER_PROFILE_U'],
											
										]
							],
							['Add',		[
											['Add College', 'RUSER_ADD_COLLEGE'],
											['Add Subject', 'RUSER_ADD_SUBJECT'],
											['Add Student', 'RUSER_STUDENT'],
											['Add Faculty', 'RUSER_ADD_FACULTY'],
											['Assign Subject','RUSER_SUBJECT'],
											['Add QPCode', 'RUSER_ADD_QP'],
											['Add Question', 'RUSER_ADD_QUESTIONS'],
										]
							],
							['Paper', 	[
											['Validation', 'RUSER_VALIDATE_PAPER'],
											['Allocation', 'RUSER_ALLOC'],
											['Upload','RUSER_UPLOAD'],
										],
							],
							['List of Teacher','RUSER_SYS1'],
							['System Information','RUSER_SYS2'],
							['Logout', 'RSYSTEM_LOGOUT'],
	];

	private $dropdown;
	private $navigation;

	/**
	 * Generates the Navigation bar.
	 * @method __construct
	 */
	function __construct($mobileView = false)
	{
		$dummy = [];
		$this->validateElement($this->elements, $dummy, $this->dropdown, $this->navigation, 0, $mobileView);
	}

	function getNavBar()
	{
		return [$this->dropdown, $this->navigation];
	}

	/**
	 * Validates the Elements of Navigation Br
	 * Also Displays it according to the role.
	 * @method validateElement
	 * @param  array           $elements   all elemnts
	 * @param  array           &$temp      true for indexes which can be shown(only for nested)
	 * @param  int             $nested     nested dropdown?
	 * @param  bool            $mobileView is it mobile View?
	 * @return int                         total number of elements shown
	 */
	private function validateElement($elements, &$temp, &$dropdown, &$navigation, $nested = 0, $mobleView = false)
	{
		global $Role, $user, $redirect_indexes;
		$total_true = 0;
		/** Loop Through all elements */
		for ($i = 0; $i < count($elements); $i++) {
			/** If Array, then validate Element and display it */
			if (is_array($elements[$i][1])) {
				$display = [];
				$true = $this->validateElement($elements[$i][1], $display, $dropdown, $navigation, $nested+1);
				if ($true > 0 && $nested == 0) {
					if ($mobleView == false) {
						$dropdown .= '<ul id="dropdown'. $i .'" class="dropdown-content">'."\n";
					} else {
						$dropdown .= '<ul id="dropdown'. $i .'_mob" class="dropdown-content">'."\n";
					}
					//<li class="divider"></li>
					for ($j = 0; $j < count($elements[$i][1]); $j++) {
						if ($display[$j] == true) {
							$dropdown .= '	<li><a href="'. getRedirectUrl($elements[$i][1][$j][1]) .'">'. $elements[$i][1][$j][0] .'</a></li>'."\n";
						}
					}
					$dropdown .= '</ul>'."\n";
					// Mobile
					if ($mobleView == false) {
						$navigation .= '<li><a class="dropdown-button" href="#!" data-activates="dropdown'. $i .'">'. $elements[$i][0] .'<i class="material-icons right">arrow_drop_down</i></a></li>'."\n";
					} else {
						$navigation .= '<li><a class="dropdown-button" href="#!" data-activates="dropdown'. $i .'_mob">'. $elements[$i][0] .'<i class="material-icons right">arrow_drop_down</i></a></li>'."\n";
					}
				} else if ($nested > 0) {
					throw new Exception("Nested Dropdown is not supported.");
				}
				$total_true += $true;
			} else {
				$redirectConstant = $elements[$i][1];
				/** Check for Role and Display */
				if ($redirect_indexes[$redirectConstant][1] == '' || in_array($redirect_indexes[$redirectConstant][1], $Role->roles[$user->role])) {
					if ($nested == 0) {
						$navigation .= '<li><a href="'. getRedirectUrl($redirectConstant) .'">'. $elements[$i][0] .'</a></li>'."\n";
					} else {
						$temp[$i] = true;
						$total_true++;
					}
				} else {
					$temp[$i] = false;
				}
			}
		}
		return $total_true;
	}
}
?>