<?php

class Socket
{
	private $address = WEB_SERVICE_IP;
	private $port = WEB_SERVICE_PORT;
	private $socket;
	function __construct()
	{
		global $message;
		$this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		if ($this->socket === false) {
			$message->addError("Unable to establish connection."); // socket_strerror(socket_last_error()) 
			redirect('RUSER_INDEX');
		}
		// Connect to Web Service
		$result = socket_connect($this->socket, $this->address, $this->port);
		if ($result === false) {
			$message->addError("Unable to establish connection to server."); // socket_strerror(socket_last_error()) 
			redirect('RUSER_INDEX');
		}
	}

	function sendData($data)
	{
		socket_write($this->socket, $data, strlen($data));
		$o = '';
		while($out = socket_read($this->socket, 2048)) {
			$o .= $out;
		}
		return $o;
	}

	function close()
	{
		socket_close($this->socket);
	}
}

?>