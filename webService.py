# python
import socket
import select
import time
import threading
import thread
# Import MySQL
import MySQLdb
# Import json
import json
import os
# QR Codes
import pyqrcode
import qrtools
from wand.image import Image
# OMR Scanning
from imutils.perspective import four_point_transform
from imutils import contours
import numpy as np
import argparse
import imutils
import cv2


PORT = 10001
MAX_CLIENTS = 10
PACKET_SIZE = 2048

# Connect to MySQL
db = MySQLdb.connect("127.0.0.1", "root", "+chxAreU.7xU", "sih")
cursor = db.cursor()

def recv_all(sock):
	'''Receive everything from `sock`, until timeout occurs, meaning sender
	is exhausted, return result as string.'''

	prev_timeout = sock.gettimeout()
	try:
		# Set Timeout to very low in case sock.recv hangs(for not getting enough data)
		sock.settimeout(0.01)

		rdata = []
		i = 0
		while True:
			try:
				data = sock.recv(PACKET_SIZE)
				if (data == ''):
					i = i + 1
				else:
					i = 0
				if (i == 10):
					return('', 1)
				rdata.append(data)
			except socket.timeout:
				if len(rdata) == 0:
					sock.settimeout(prev_timeout)
					return ('', 1)
				else:
					sock.settimeout(prev_timeout)
					return (''.join(rdata), 0)

		# unreachable
	finally:
		# Set back to previous timeout
		sock.settimeout(prev_timeout)

# create an INET, STREAMing socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Bind to public host
sock.bind(('', PORT))
# Listen upto 5 connection request at a time.
sock.listen(MAX_CLIENTS)

SOCKET_LIST = list()
SOCKET_LIST.append(sock)


print("Running at "+ socket.gethostname())

def QRCode(dir_name):
	qr2 = qrtools.QR()
	qr2.decode(dir_name)
	jdata = None
	if qr2 is None:
		# Invalid QR Code, Skip it for now
		# @todo Insert into sql
		return (False, '', jdata)
	try:
		jdata = json.loads(qr2.data)
		qp_code = jdata['qp_code']
		studId = jdata['studId']
		dir_name = "uploads/paper/"+ str(qp_code) +"/"+ str(studId) +"/"
		print(jdata)
		if not os.path.exists(dir_name):
			os.makedirs(dir_name)
	except Exception as e:
		#print("Error", e)
		pass
	return (True, dir_name, jdata)

def OMRScan(file_name):
	image = cv2.imread(file_name)

	# Initialize Question Variables
	main_question_number = -1
	sub_questions = list()

	# 2 OMR to be scanned
	for omr_i in range(0, 2):
		if (omr_i == 1):	# Check if 2nd OMR is filled.
			omrs = image[0:1600,0:250]
		else:
			omrs = image[1601:,0:250]

		gray = cv2.cvtColor(omrs, cv2.COLOR_BGR2GRAY)
		blurred = cv2.GaussianBlur(gray, (5, 5), 0)
		edged = cv2.Canny(blurred, 75, 200)
		paper = omrs
		warped = gray

		# apply Otsu's thresholding method to binarize the warped
		# piece of paper
		thresh = cv2.threshold(warped, 0, 255,
			cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

		# find contours in the thresholded image, then initialize
		# the list of contours that correspond to questions
		cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
			cv2.CHAIN_APPROX_SIMPLE)
		cnts = cnts[0] if imutils.is_cv2() else cnts[1]
		questionCnts = []

		# loop over the contours
		for c in cnts:
			# compute the bounding box of the contour
			(x, y, w, h) = cv2.boundingRect(c)
			ar = w / float(h)

			# Aspect Ratio
			if w >= 40 and h >= 40 and ar >= 0.8 and ar <= 1.3:
				questionCnts.append(c)

		# sort the question contours top-to-bottom
		questionCnts = contours.sort_contours(questionCnts, method="top-to-bottom")[0]

		if (len(questionCnts)/2 != 9):
			# 9 Questions not scanned.
			continue
		# 2 omr each line
		for (q, i) in enumerate(np.arange(0, len(questionCnts), 2)):
			# sort the contours for the current question
			cnts = contours.sort_contours(questionCnts[i:i + 2])[0]
			bubbled = None
			bubble_number = 0
			# loop over the sorted contours
			for (j, c) in enumerate(cnts):
				bubble_number = bubble_number + 1
				# Current Bubble
				mask = np.zeros(thresh.shape, dtype="uint8")
				cv2.drawContours(mask, [c], -1, 255, -1)

				# Non Zero
				mask = cv2.bitwise_and(thresh, thresh, mask=mask)
				total = cv2.countNonZero(mask)

				# Check Totalled Black Area.
				if total > 2000:
					# Main Question
					if (bubble_number == 1):
						# Main Question Already Marked? so probably multiple marking.
						if (main_question_number != -1):
							# Multiple Marked check other omr. skip this one.
							pass
						else:
							main_question_number = q
					else:
						# SubQuestion Marked.
						sub_questions.append(q)

		# 2nd OMR and main question is marked, so it implies that
		# first OMR was filled wrong, thus seconf omr was filled.
		if (omr_i == 0 and main_question_number != -1):
			break
	return (main_question_number, sub_questions)


def ImageProcess(data, dummy):
	sql = "SELECT `id`, `name` FROM `file_process`"
	# Fetch Results
	cursor.execute(sql)
	results = cursor.fetchall()
	paper_dir = 'uploads/paper/'
	for row in results:
		print("Processing File: "+ row[1])
		useNewDir = False
		file_name = row[1]
		im = Image(filename='uploads/tmp/'+ file_name, resolution=350)
		continuePage = True
		page_q = dict()
		for i, page in enumerate(im.sequence):
			print("\tPage: "+ str(i))
			with Image(page) as page_image:
				if (useNewDir == False):
					dir_name = paper_dir + '' + str(i) +'_'+ file_name[:-4] +".png"
				else:
					dir_name = paper_dir + '' + str(i) +".png"

				page_image.save(filename=dir_name)
				# Parse QR Code
				if (i == 0):
					(continuePage, paper_dir, jdata) = QRCode(dir_name)
					if (continuePage == False):
						break
					useNewDir = True
				elif (i >= 2):
					# Parse OMR code from 3rd page (2nd page is rule)
					(main_question_number, sub_questions) = OMRScan(dir_name)
					page_q[i] = dict()
					page_q[i]['main'] = main_question_number
					page_q[i]['sub'] = sub_questions
					print("Question Attempted: ", main_question_number)
					print("Sub Questions: ", sub_questions)

				# Remove the First Page and Rules Page.
				if (i <= 1):
					os.remove(dir_name)
		# Cannot Parse QR Code for Some Reason
		if (continuePage == False or jdata is None):
			print("Cannot Parse")
			del page_q
			continue
		# Insert Paper ID
		sql = 'INSERT INTO `paper` (`student_id`, `qp_code`, `status`) VALUES (\''+ str(jdata['studId']) +'\', \''+ str(jdata['qp_code']) +'\', \'0\')'
		try:
			cursor.execute(sql)
			db.commit()
		except:
			print("Paper Already Uploaded.")
			db.rollback()
			# Rollback, Already Inserted, so delete from file_process.
			sql = 'DELETE FROM `file_process` WHERE `id`=\'%d\'' % (row[0])
			cursor.execute(sql)
			db.commit()
			del page_q
			continue
		last_id = cursor.lastrowid
		print("Inserted ID:", last_id)
		# Delete from File Process
		sql = 'DELETE FROM `file_process` WHERE `id`=\'%d\'' % (row[0])
		cursor.execute(sql)
		db.commit()
		print("File "+ str(row[1]) +" Processed.")
		# Insert Questions
		for pno, value in page_q.iteritems():
			for sub_i in range(len(value['sub'])):
				sql = 'INSERT INTO `paper_attempted` (`paper_id`, `page_no`, `question`, `sub_question`) VALUES (\'%d\', \'%d\', \'%d\', \'%d\')' % (last_id, pno, value['main'], value['sub'][sub_i])
				cursor.execute(sql)
				db.commit()
		del page_q
		# Enter The Questions
		

#ImageProcess('20170401_164617_3paper', 1)
while 1:
	# Get Read Sockets
	# 5 Seconds Timeout
	# Accept Connection
	(client, address) = sock.accept()
	print "Client("+ address[0] +") Connected at "+ time.strftime('%a, %d %b %Y %H:%M:%S') +"."
	#client = csockfd
	(addr, port) = client.getpeername()
	addridx = str(addr) +":"+ str(port)
	# Get the 0Data (Returns 0 length if timeout)
	#(data, error) = recv_all(client)
	data = client.recv(PACKET_SIZE)
	error = 0
	# Some Error, Close the connection.
	if (error > 0 or len(data) == 0):
		client.close()
		continue
	# Normalize the Data
	data = ''.join((line + '\n') for line in data.splitlines())

	data = data.strip()
	print(data[:13], data[13:])
	if (data[:12] == 'TEMP_PROCESS'):
		print("Processing...")
		thread.start_new_thread(ImageProcess, (data, 1))

	# Send the Data
	client.send('DONE')
	# Check if to disconnect the client or not
	client.close()

sock.close()
cursor.close()
cursor2.close()