// Paper ScollBar
var scrolled = 0;
var scrolling = false;
var lastNumber = -1;
// Stop animation in case mousewheel is used.
$(window).bind("mousewheel", function() {
	$("#p1").stop();
});
// Disable Context Menu for #p1
document.oncontextmenu = function() {
	return false;
};
// Right click for P1
$("#p1").mousedown(function(e){ 
	if (e.button == 2) {
		e.preventDefault();
		console.log("Checked Sign.");
		return true; 
	} 
	return true; 
});
// Scroll by Other means?, then just set the scrolled value, to be in sync.
$("#p1").scroll(function() {
	if (!scrolling) {
		scrolled = $("#p1").scrollTop();
		ChangeQuestion(scrolled, height_of_canvas);
	}
});
// Scroll with up/down button
$(document).keydown(function(e){
	// Number Pressed?
	var numberPressed = -1;
	// Scrolled?
	var scr = false;
	// Max Scrollable Height
	var maxScrollTop = $("#p1").prop("scrollHeight") - $("#p1").outerHeight();
	// Bind Up and Down
	switch(e.keyCode) {
		//case 37: // left
		case 38: // up
			scrolled -= 100;
			scr = true;
			break;
		//case 39: // right
		case 40: // down
			scrolled += 100;
			scr = true;
			break;
	}
	// Bind Numbers
	if (e.keyCode >= 48 && e.keyCode <= 57) {
		numberPressed = e.keyCode-48;
	} else if (e.keyCode >= 96 && e.keyCode <= 105) {
		numberPressed = e.keyCode-96;
	}

	// Number is Pressed.
	if (numberPressed != -1) {
		lastNumber = numberPressed;
		e.preventDefault();
		console.log("Marks to Give: "+ numberPressed);
		var r3 = document.getElementById('r3')
		r3.innerHTML = lastNumber +' Marks would be given upon click';
	}

	// Animate and preventDefault Action
	if (scr) {
		scrolling = true;
		var currentScrolled = $("#p1").scrollTop();
		// Limit the Scroll from 0-maxHeight
		if (scrolled < 0) {
			scrolled = 0;
		} else if (scrolled > maxScrollTop) {
			scrolled = maxScrollTop;
		}
		var temp_check = scrolled;
		e.preventDefault();
		// Stop Animation
		$("#p1").stop();
		// Current Scrolled should not be scrolled value, that means no change
		if (currentScrolled != scrolled) {
			ChangeQuestion(scrolled, height_of_canvas);
			$("#p1").animate({
				scrollTop: scrolled
			},
			{
				complete: function() {
					if (temp_check == scrolled) {
						scrolling = false;
					}
				}
			});
		}
	}
});