(function($){
	$(function(){
		$('.button-collapse').sideNav();
		$('.dropdown-button').dropdown();
		$('select').material_select();
	});

	// IP Lock 
	$('#iplock').change(function() {
		if (this.checked) {
			$('.ip').prop("disabled", true);
		} else {
			$('.ip').prop("disabled", false);
		}
	});
})(jQuery);
