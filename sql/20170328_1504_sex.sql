# Defines the Sex of the User

ALTER TABLE `login` ADD `sex` ENUM('M','F','O') NOT NULL DEFAULT 'M' COMMENT 'Sex of User' AFTER `email`;
