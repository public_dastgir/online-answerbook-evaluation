# Student and their Subjects
CREATE TABLE `student` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(50) NOT NULL,
 `seat_no` int(11) NOT NULL,
 `c_code` int(11) NOT NULL,
 `c_name` varchar(50) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `student_sub` (
 `sub_id` int(11) NOT NULL AUTO_INCREMENT,
 `sub_name` varchar(100) NOT NULL,
 `student_id` int(11) NOT NULL,
 PRIMARY KEY (`sub_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
