--
-- Table structure for table `sql_version`
--
DROP TABLE IF EXISTS `sql_version`;
CREATE TABLE IF NOT EXISTS `sql_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

INSERT INTO `sql_version` VALUES (1, '1-base.sql');

--
-- Table structure for table `login`
--
DROP TABLE IF EXISTS `login`;
CREATE TABLE IF NOT EXISTS `login` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'User Unique Login ID',
  `username` varchar(50) NOT NULL COMMENT 'Username',
  `password` varchar(255) NOT NULL COMMENT 'Hashed Password',
  `name` varchar(100) NOT NULL COMMENT 'Full Name',
  `email` varchar(100) NOT NULL COMMENT 'Email Address',
  `role` int(11) NOT NULL COMMENT 'Role',
  `verified` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Verified email?',
  `last_login` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Last Login',
  `ip_locked` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'is Account IP Locked?',
  `ip` varchar(16) NOT NULL COMMENT 'IP Address of locked',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
