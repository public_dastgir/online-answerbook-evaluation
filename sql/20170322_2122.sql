# 21/03/2017 21:22
# Adds Unique ID to login table for unique session

ALTER TABLE `login` ADD `unique_id` VARCHAR(128) NOT NULL COMMENT 'Randomly generated per session.' AFTER `verified`;
