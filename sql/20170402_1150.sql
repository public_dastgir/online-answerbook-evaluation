# Questions
CREATE TABLE `questions` ( `id` INT NOT NULL AUTO_INCREMENT , `qp_code` VARCHAR(128) NOT NULL , `main_question` INT NOT NULL DEFAULT '1' , `sub_question` CHAR(1) NOT NULL DEFAULT 'a' , `marks` INT NOT NULL DEFAULT '3' , `question` TEXT NOT NULL , PRIMARY KEY (`id`), UNIQUE `question` (`qp_code`, `main_question`, `sub_question`)) ENGINE = InnoDB;
