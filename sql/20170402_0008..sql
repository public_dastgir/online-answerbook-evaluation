# Paper Database
CREATE TABLE `paper` ( `paper_id` INT NOT NULL AUTO_INCREMENT COMMENT 'Auto Incremented ID, Paper ID' , `student_id` INT NOT NULL COMMENT 'Student ID' , `qp_code` VARCHAR(255) NOT NULL COMMENT 'QP Code' , `status` INT NOT NULL COMMENT 'Status of Paper' , PRIMARY KEY (`paper_id`), UNIQUE `qp_student` (`student_id`, `qp_code`)) ENGINE = InnoDB;
