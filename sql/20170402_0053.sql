# Paper Attempted
CREATE TABLE `paper_attempted` ( `id` INT NOT NULL AUTO_INCREMENT COMMENT 'Auto Incremented' , `paper_id` INT NOT NULL COMMENT 'Paper ID' , `page_no` INT NOT NULL DEFAULT '1' COMMENT 'Page Number' , `question` INT NOT NULL DEFAULT '0' COMMENT 'Question Number' , `sub_question` INT NOT NULL DEFAULT '0' COMMENT 'Sub Question' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
