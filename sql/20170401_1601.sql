# Paper Allocation ID.
CREATE TABLE `allocates` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `pid` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `marks` int(11) NOT NULL,
  `tid` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
