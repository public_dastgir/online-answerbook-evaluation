# Contains College Records

CREATE TABLE IF NOT EXISTS `college` (
  `id` int(11) NOT NULL COMMENT 'College ID',
  `name` varchar(128) NOT NULL COMMENT 'College Name'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='College Names';
