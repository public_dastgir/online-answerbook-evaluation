# Add ID as Primary Auto Increment Key for College
ALTER TABLE `college` ADD PRIMARY KEY(`id`);
ALTER TABLE `college` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'College ID';
