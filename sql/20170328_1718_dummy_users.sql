# Dummy Users and Colleges

INSERT INTO `login` (`id`, `username`, `password`, `name`, `email`, `sex`, `role`, `college_id`, `verified`, `unique_id`, `last_login`, `ip_locked`, `ip`) VALUES (NULL, 'teacher', '$2y$10$U1.eq.XYBiN.0QizmlWneep9n8t1w0.GNZvfRjoAZtgDfaHj6UXvK', 'Teacher 1', 'a@a.com', 'M', '1', '1', '1', '', CURRENT_TIMESTAMP, '0', '');
INSERT INTO `login` (`id`, `username`, `password`, `name`, `email`, `sex`, `role`, `college_id`, `verified`, `unique_id`, `last_login`, `ip_locked`, `ip`) VALUES (NULL, 'cadmin', '$2y$10$U1.eq.XYBiN.0QizmlWneep9n8t1w0.GNZvfRjoAZtgDfaHj6UXvK', 'College Admin 1', 'a@a.com', 'M', '2', '1', '1', '', CURRENT_TIMESTAMP, '0', '');
INSERT INTO `login` (`id`, `username`, `password`, `name`, `email`, `sex`, `role`, `college_id`, `verified`, `unique_id`, `last_login`, `ip_locked`, `ip`) VALUES (NULL, 'staff', '$2y$10$U1.eq.XYBiN.0QizmlWneep9n8t1w0.GNZvfRjoAZtgDfaHj6UXvK', 'University Staff 1', 'a@a.com', 'M', '4', '0', '1', '', CURRENT_TIMESTAMP, '0', '');
INSERT INTO `login` (`id`, `username`, `password`, `name`, `email`, `sex`, `role`, `college_id`, `verified`, `unique_id`, `last_login`, `ip_locked`, `ip`) VALUES (NULL, 'admin', '$2y$10$U1.eq.XYBiN.0QizmlWneep9n8t1w0.GNZvfRjoAZtgDfaHj6UXvK', 'Univeristy Admin 1', 'a@a.com', 'M', '8', '0', '1', '', CURRENT_TIMESTAMP, '0', '');

INSERT INTO `college` (`id`, `name`) VALUES (1, 'A'), (2, 'B'), (3, 'C'), (4, 'D');
