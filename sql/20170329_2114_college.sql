#Add Registration NUMBER
ALTER TABLE `college` ADD `reg_no` VARCHAR(50) NOT NULL COMMENT 'Registration Number for college' AFTER `name`;

UPDATE `college` SET `reg_no`=`id`;

ALTER TABLE `college` ADD UNIQUE(`reg_no`);
