# Paper Codes
DROP TABLE question_paper;

CREATE TABLE `paper_codes` ( `id` INT NOT NULL AUTO_INCREMENT , `qp_code` INT NOT NULL , `subject_id` INT NOT NULL , PRIMARY KEY (`id`), UNIQUE (`qp_code`)) ENGINE = InnoDB;