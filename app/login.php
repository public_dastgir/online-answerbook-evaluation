<html>
<head>
	<title>Login Form</title>
	<link rel="stylesheet" href="<?php echo getRedirectUrl('RCSS_LOGIN'); ?>">
</head>

<body>
	<form method="POST" action="<?php echo getRedirectUrl('RSYSTEM_LOGIN'); ?>">
		<div class="login-wrap">
			<div class="login-html">
<?php
				$errors = $message->getError();
				if (!empty($errors)) {
					print "Error:<br/>";
					foreach ($errors as $error) {
						print $error."<br/>";
					}
					$message->clearError();
				}
?>
				<br/><br/>
				<input id="tab-1" type="radio" class="sign-in" checked><label for="tab-1" class="tab">Sign In</label>
				
				<div class="login-form">
					<div class="sign-in-htm">
						<div class="group">
							<label for="user" class="label">Username</label>
							<input id="user" type="text" name="username" class="input">
						</div>
						<div class="group">
							<label for="pass" class="label">Password</label>
							<input id="pass" type="password" name="password" class="input" data-type="password">
						</div>
						<div class="group">
							<input id="check" type="checkbox" class="check" name="signed_in" checked>
							<label for="check"><span class="icon"></span> Keep me Signed in</label>
						</div>
						<div class="group">
							<input type="submit" class="button" value="Submit">
						</div>
						<div class="hr">
							
						</div>
						<div class="foot-lnk">
							<a href="#forgot">Forgot Password?</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</body>
</html>
