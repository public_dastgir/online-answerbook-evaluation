<div class="row">
	<form method="POST" action="<?php echo getRedirectUrl("RSYSTEM_UPDATE_PROFILE"); ?>" class="col s12">
		<div class="row">
			<div class="input-field col s6">
				<input disabled value="<?php echo $user->name; ?>" id="name" type="text">
				<label for="name">Name</label>
			</div>
			<div class="input-field col s6">
				<input disabled value="<?php echo $user->email; ?>" id="email" type="email">
				<label for="email">Email</label>
			</div>
			<div class="input-field col s6">
				<input placeholder="<?php echo $user->mobile_no; ?>" name="mob_no" id="contact" type="tel" class="validate">
				<label for="contact">Contact</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input required name="password" id="password" type="password" class="validate">
				<label for="password">Current Password</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s6">
				<input name="npassword" id="npassword" type="password" class="validate">
				<label for="vpassword">New Password</label>
			</div>
			<div class="input-field col s6">
				<input name="vpassword" id="vpassword" type="password" class="validate">
				<label for="npassword">Verify Password</label>
			</div>
		</div>
		<div class="row center">
			<button class="btn waves-effect waves-light" type="submit">Update
				<i class="material-icons right">update</i>
			</button>
		</div>
	</form>
</div>
