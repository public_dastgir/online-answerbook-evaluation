<div class="row">
	<form class="col s12">
		<div class="row">
			<div class="input-field col s6">
				<input disabled placeholder="<?php echo $user->name; ?>" name="name" id="name" type="text" ">
				<label for="name">Name</label>
			</div>
			<div class="input-field col s6">
				<input disabled placeholder="<?php echo $user->email; ?>" name="email" id="email" type="email" ">
				<label for="email">Email</label>
			</div>
			<div class="input-field col s6">
				<input disabled placeholder="<?php echo $user->mobile_no; ?>" name="mob_no" id="contact" type="tel" ">
				<label for="contact">Contact</label>
			</div>
		</div>
		<p>
			Gender:
			<input class="with-gap" <?php if ($user->sex == 'M') { echo 'checked'; } ?> disabled name="sex" type="radio" id="male" value="M" />
			<label for="male">Male</label>
			<input class="with-gap" <?php if ($user->sex == 'F') { echo 'checked'; } ?> disabled name="sex" type="radio" id="female" value="F" />
			<label for="female">Female</label>
			<input class="with-gap" <?php if ($user->sex == 'O') { echo 'checked'; } ?> disabled name="sex" type="radio" id="others" value="O" />
			<label for="others">Others</label>
		</p>
		<div class="input-field col s6">
			<select disabled name="college">
				<?php
					$name = $DB->getColleges($user->college_id);
					echo "<option disabled selected value='{$user->college_id}'>{$name}</option>";
				?>
			</select>
			<label>College</label>
		</div>
		<div class="input-field col s6">
			<select disabled " name="role">
				<?php
					// University Admin can add anyone
					for ($i = 0; $i < count($Role->titles); $i = $i + 2) {
						if ($Role->titles[$i] == $user->role) {
							echo "<option selected value='{$Role->titles[$i]}'>{$Role->titles[$i+1]}</option>";
							break;
						}
					}
				?>
			</select>
			<label>Roles</label>
		</div>
		<div class="row">
			<div class="input-field col s2 offset-s5">
				<input type="checkbox" disabled name="ip_locked" <?php if ($user->ip_locked == true) { echo "checked"; } ?> id="iplock"/>
				<label for="iplock">IP Locked?</label>
			</div>
		</div>
	</form>
</div>
