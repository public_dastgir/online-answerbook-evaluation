<div class="row">
	<form method="POST" action="<?php echo getRedirectUrl("RSYSTEM_ADD_FACULTY"); ?>" class="col s12">
		<div class="row">
			<div class="input-field col s6">
				<input required placeholder="Name" name="name" id="name" type="text" class="validate">
				<label for="name">Name</label>
			</div>
			<div class="input-field col s6">
				<input required placeholder="a@a.com" name="email" id="email" type="email" class="validate">
				<label for="email">Email</label>
			</div>
			<div class="input-field col s6">
				<input required placeholder="9123456789" name="mob_no" id="contact" type="tel" class="validate">
				<label for="contact">Contact</label>
			</div>
		</div>
		<p>
			Gender:
			<input class="with-gap" checked name="sex" type="radio" id="male" value="M" />
			<label for="male">Male</label>
			<input class="with-gap" name="sex" type="radio" id="female" value="F" />
			<label for="female">Female</label>
			<input class="with-gap" name="sex" type="radio" id="others" value="O" />
			<label for="others">Others</label>
		</p>
		<div class="input-field col s6">
			<select name="college">
				<option value="" disabled selected>Choose the College</option>
				<?php
					if ($user->role == 8) {
						$college = $DB->getColleges();
						$selected = "selected";
						foreach ($college as $id => $name) {
							echo "<option $selected value='{$id}'>{$name}</option>";
							$selected = '';
						}
					} else {
						$name = $DB->getColleges($user->college_id);
						echo "<option selected value='{$user->college_id}'>{$name}</option>";
					}
				?>
			</select>
			<label>College</label>
		</div>
		<div class="input-field col s6">
			<select required class="validate" name="role">
				<option value="" disabled>Choose Role</option>
				<?php
					// University Admin can add anyone
					$roles = [];
					$selected = "selected";
					for ($i = 0; $i < count($Role->titles); $i = $i + 2) {
						if ($Role->titles[$i] < $user->role) {
							echo "<option $selected value='{$Role->titles[$i]}'>{$Role->titles[$i+1]}</option>";
							$selected = '';
						}
					}

				?>
			</select>
			<label>Roles</label>
		</div>
		<div class="row">
<?php
			$ip = inet_pton($user->ip);
			//$ip = inet_pton("127.0.0.1");
			$size = strlen($ip);
			if ($size == 16) {	// IPv6
				$hex = unpack("H32", $ip);
				$hex = $hex[1];
				for ($i = 0; $i < 8; $i++) {
					$ip4 = $hex[$i*4+0] . $hex[$i*4+1] . $hex[$i*4+2] . $hex[$i*4+3];
					echo '<div style="width: 4.33%" class="input-field col s1">
							<input required value="'. strtoupper($ip4) .'"';
					if ($i != 7 && $user->role != 8) {
						echo 'disabled class="validate ip" ';
					} else {
						echo 'placeholder="IP'. ($i+1) .'" class="validate" ';
					}
					echo ' name="ip'. ($i+1) .'" id="ip'. ($i+1) .'" type="text" class="validate ip">
							<label for="ip'. ($i+1) .'">IP'. ($i+1) .'</label>
						</div>';
				}
			} else if ($size == 4) {
				$num = unpack("C4", $ip);
				for ($i = 0; $i < strlen($ip); $i++) {
					echo '<div class="input-field col s1">
							<input required value="'. $num[$i+1] .'"';
					if ($i != strlen($ip)-1 && $user->role != 8) {
						echo 'disabled class="validate ip" ';
					} else {
						echo 'placeholder="IP'. ($i+1) .'" class="validate" ';
					}
					echo 'name="ip'. ($i+1) .'" id="ip'. ($i+1) .'" type="number">
							<label for="ip'. ($i+1) .'">IP'. ($i+1) .'</label>
						</div>';
				}
			} else {
				throw new Exception("Unknown IP Format");
			}
?>
			<div class="input-field col s2">
				<input type="checkbox" name="ip_locked" <?php if ($user->ip_locked) { echo "selected disabled"; } ?> id="iplock" checked="checked" />
				<label for="iplock">IP Locked?</label>
			</div>
			<div class="input-field col s4 center">
				<input disabled placeholder="You can only change the last octact of the IP if locked." id="note" type="text">
				<label for="note">NOTE</label>
			</div>
		</div>


		<div class="row center">
			<button class="btn waves-effect waves-light" type="submit">Add Member
				<i class="material-icons left">add</i>
			</button>
		</div>
	</form>
</div>
