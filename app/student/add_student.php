<?php
 
$sub="select `subject` from `subject`";
$result = $DB->query($sub);
echo "<form action=' ". getRedirectUrl("RUSER_STUDENT_DB") ." ' method='post'>";
/*echo '<table width="100%">
				<tr><center><h2>Student Details</h2></center></tr>
                <tr>
                    <td><b>Name</b></td>
                    <td><b>Seat no:</b></td>
                    <td><b>College_code</b></td>
                    <td><b>College</b></td>
                    <td><b>Subjects</b></td>
                </tr>';
 */
 ?>
<div class="row" >
			<div class="input-field col s3">
				<input required name="name" id="name" type="text" class="validate">
				<label for="name">Name</label>
			</div>
			<div class="input-field col s3">
				<input required name="seatno" id="seatno" type="text" class="validate">
				<label for="seatno">Seat No.</label>
			</div>
			<div class="input-field col s3">
				<select name="college_code">
					<option value="" disabled selected>Choose College</option>
					<?php
						if ($user->role == 8) {
							$college = $DB->getColleges();
							$selected = "selected";
							foreach ($college as $id => $name) {
								echo "<option $selected value='{$id}'>{$name}</option>";
								$selected = '';
							}
						} else {
						$name = $DB->getColleges($user->college_id);
						echo "<option selected value='{$user->college_id}'>{$name}</option>";
						}
					 ?>
				</select>
			<label>College</label>
			</div>
			<div class="input-field col s3">
				<select name='sel[]' id='sel[]' multiple>
			<?php
			while($row = $result->fetch_assoc()) {
				echo "<option value='".$row['subject']."'>".$row['subject']."</option>";
			}
			?>
			</select>
			<label for='sel[]'>subject</label>
			</div>
		</div>
		<div class="row center"><button class="btn waves-effect waves-light" type="submit">Add Student<i class="material-icons left">add</i></button></div>
</form>