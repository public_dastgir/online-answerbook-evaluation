<form method="POST" action="<?php echo getRedirectUrl("RSYSTEM_ADD_SUBJECT"); ?>" class="col s12">
	<div class="row">
		<div class="input-field col s12">
			<input  name="subject" id="subject" type="text" class="validate">
			<label for="subject">Subject</label>
		</div>
	</div>
	<div class="row center">
		<button class="btn waves-effect waves-light" type="submit">Add Subject
			<i class="material-icons left">add</i>
		</button>
	</div>
</form>