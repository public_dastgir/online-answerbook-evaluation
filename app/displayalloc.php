<form method="post" action='<?php echo getRedirectUrl("RSYSTEM_ALLOC"); ?>'>
<?php
	$result = $DB->query("SELECT `id` FROM `allocates` WHERE `tid`='{$user->id}'");
	if ($result = NULL && $result->num_rows > 0) {
		$message->addError("You already have Allocated Papers.".$user->id);
		redirect('RUSER_INDEX');
	}
	$s = '';
	if (count($user->subjects)!=0) {
		for ($i = 0; $i < count($user->subjects); $i++) {
			$s .= "'{$user->subjects[$i]}'";
			if ($i != count($user->subjects)-1) {
				$s .= ",";
			}
		}
	} else {
		$message->addError("No Subjects Allocated, Try Again Later.");
		redirect('RUSER_INDEX');
	}
	$query = "SELECT COUNT(`p`.`paper_id`) AS `count`, `p`.`paper_id`, `s`.`subject`, `s`.`id` FROM `paper` `p` LEFT JOIN `paper_codes` `pc` ON `pc`.`qp_code`=`p`.`qp_code` AND `pc`.`subject_id` IN (". $s .") LEFT JOIN `subject` `s` ON `s`.`id`=`pc`.`subject_id` WHERE `p`.`status`='0' GROUP BY `p`.`paper_id`";
	$result = $DB->query($query);

?>
	<select name="allocate">
		<option disabled selected>Select Subject to Allocate</option>
<?php
		while($row=mysqli_fetch_assoc($result)){

?>
			<option value="<?php echo($row['id'])?>"><?php echo($row['subject']."(Available in Pool: {$row['count']})"); ?></option>

<?php
		}
?>
	</select>
	<br/>
	<div class="row center">
		<button class="btn waves-effect waves-light" type="submit">Allocate
			<i class="material-icons left">add</i>
		</button>
	</div>
</form>

<br><br><br>


<form method="post"  action='<?php echo getRedirectUrl("RSYSTEM_DEALLOC"); ?>'>
<div class="row center">
			<button class="btn waves-effect waves-light" type="submit">Deallocate
				<i class="material-icons left">send</i>
			</button>
		</div>
</form>