<form action='<?php echo getRedirectUrl('RSYSTEM_ADD_QP'); ?>' method='POST'>
	<div class="input-field col s6">
		<input id="qp_code" name="qp_code" type="text" class="validate">
		<label for="qp_code">Question Paper Code(QP Code)</label>
	</div>
	<div class="input-field col s6">
		<select name="subject">
			<option value="" disabled selected>Choose the Subject</option>
			<?php
				$sql = 'SELECT `id`,`subject` FROM `subject`';
				$result = $DB->query($sql);

				$selected = "selected";
				while ($row = $result->fetch_assoc()) {
					echo "<option value='{$row['id']}'>{$row['subject']}</option>";
				}
			?>
		</select>
		<label>Subject Name</label>
	</div>
	<div class="row center">
		<button class="btn waves-effect waves-light" type="submit">Add QP Code
			<i class="material-icons right">add</i>
		</button>
	</div>
</form>

