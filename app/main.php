<?php
require_once("../config/main.php");

function getIncludeFile()
{
	global $redirect_indexes, $Role, $user, $MAIN_title, $MAIN_role, $request_methods;
	// Check Authentication and Role of User
	if (isset($_SESSION) && isset($_SESSION['auth'])) {
		$auth = $_SESSION['auth'];
		$userRole = 0;
		if (isset($_SESSION['user'])) {
			/** Debug Remove */
			if (gettype($_SESSION['user']) == "array") {
				session_destroy();
			}
			$user = unserialize($_SESSION['user']);
		}
	} else {
		$auth = false;
	}
	// Get the requested url and method
	$url = $_SERVER['REQUEST_URI'];
	$method = $_SERVER['REQUEST_METHOD'];
	// Remove '/BASE_DIR_NAME/' from URL
	$subUrl = substr($url, strlen(SIH_DIR_NAME));
	$passed = false;
	$userTitle = -1;
	// Check All Redirect Indexes to find best possible match.
	foreach ($redirect_indexes as $constant => $redirect_to) {
		$testUrl = '/^'.str_replace('/', '\/', $redirect_to[0]).'$/';
		$roleRequired = $redirect_to[1];
		// Assume Failed
		$passed = false;
		// Title of Page
		if ($redirect_to[2] != '') {
			$MAIN_title = $redirect_to[2];
		}
		// Match the String.
		if (preg_match($testUrl, $subUrl)) {
			// Match Request Methods
			if ($method != $request_methods[$constant]) {
				$passed = false;
				break;
			}
			// If Method Doesn't match, error404 page.
			$MAIN_role = $roleRequired;
			// no role required, anyone can pass.
			if ($roleRequired == '') {
				// If Logged in, redirect to profile
				if ($auth == true) {
					redirect('RUSER_PROFILE');
				}
				$passed = true;
				break;
			}

			// Role is required, check if logged in.
			if ($auth != true) {
				redirect('RUSER_INDEX');
			}

			// Get User Roles
			if ($user->role != 0 && in_array($roleRequired, $Role->roles[$user->role])) {
				$passed = true;
				break;
			} else {
				$passed = false;
				break;
			}
		}
	}
	if ($passed == false) {	// Invalid Page/Unauthorized
		return('error404.php');
	} else {
		// Authorized, get the Template Name.
		$fileToInclude = substr($subUrl, 0, -1).'.php';
		// If File doesn't exist, show 404 page.
		if (!file_exists($fileToInclude)) {
			return('error404.php');
		} else {
			// Include the Template
			return($fileToInclude);
		}
	}
}

$MAIN_title = "Paper Evaluation System";
$MAIN_role = '';
/// Start Session
session_start();
/// Initialize DB
$Role = new Role();
$DB = new DB();
$message = new Message();
$user = new User();

if (defined('SQL_AUTO_UPDATE') && SQL_AUTO_UPDATE == true) {
	$SQLAutoUpdater = new SQLAutoUpdater();
}

$fileName = getIncludeFile();
if ($MAIN_role != '')
	include 'header.php';
/// Include the File
include $fileName;
if ($MAIN_role != '')
	include 'footer.php';

/// Close DB
$DB->close();
?>
