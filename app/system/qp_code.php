<?php
if (!checkPost(["qp_code", "subject"])) {
	$message->addError("Unable to Add Subject.");
	redirect('RUSER_ADD_QP');
}
$qp_code = $DB->escape_string($_POST['qp_code']);
$subject_id = $_POST['subect'];
$query = "INSERT INTO `paper_codes` (`qp_code`, `subject_id`) VALUES ('{$qp_code}', '{$subject_id}')";
if ($DB->query($query)) {
	$message->addInfo("QP Code Added Successfully.");
} else {
	$message->addError("Unable to Add QP Code. Possibly Duplicate QP Code Detected.");
}

redirect('RUSER_ADD_QP');

?>