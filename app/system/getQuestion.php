<?php
function numToLet($number) {
	$char = ord('a')+$number;
	return chr($char);
}

if (isset($_POST)) {
	$paperId = $DB->escape_string($_POST['paper_id']);
	$qp_code = $DB->escape_string($_POST['qp_code']);
	$pageNo = intval($_POST['page_no'])+2;
	$query = "SELECT `p`.`question`, `p`.`sub_question`, `q`.`question` AS `t`, `q`.`marks` FROM `paper_attempted` `p` LEFT JOIN `questions` `q` ON `q`.`qp_code`='{$qp_code}' AND `p`.`question`=`q`.`main_question`-1 AND `p`.`sub_question`=`q`.`sub_question`-1 WHERE `p`.`paper_id`='{$paperId}' AND `p`.`page_no`='{$pageNo}'";
	$result = $DB->query($query);
	$q = '';
	if ($result != NULL && $result->num_rows > 0) {
		$q .= '';
		while ($row = $result->fetch_assoc()) {
			$q .= "<b>Question ". ($row['question']+1) .".". numToLet(intval($row['sub_question'])) ."</b>:<br/>";
			$q .= '<b><i>Marks: '. $row['marks'] .'</b></i><br/>';
			$q .= htmlspecialchars($row['t'])."<br/><br/>";
		}
	}
	echo "<b id='www'>". $result->num_rows ."</b>";
	echo "<b id='wwww'>";
	echo "<i><b>Page Number</b></i>: ". ($pageNo-1) ."<br/>
				<h5>Questions Attempted:</h5>
				{$q}";
	echo "</b>";
}
?>