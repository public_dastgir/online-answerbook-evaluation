<?php
ini_set('error_reporting', E_ALL);

if(isset($_FILES['file-1'])) {
	$csize = count($_FILES['file-1']['name']);
	for ($i = 0; $i < $csize; $i++) {
        if ($_FILES['file-1']['error'][$i]) {
            $message->addError("File: ". $_FILES['file-1']['name'][$i] ." has error.");
            $message->addError("Please Fix it and then upload again.");
            redirect('RUSER_UPLOAD');
        }
        if ($_FILES['file-1']['type'][$i] != 'application/pdf') {
            $message->addError("You can only upload pdf file.");
            redirect('RUSER_UPLOAD');
        }
        if ($_FILES['file-1']['size'][$i] <= 0) {
            $message->addError("Empty File Uploaded");
            redirect('RUSER_UPLOAD');
        }
    }
    $fileHandler = new FileHandler(FILETYPE_TEMP);
    for ($i = 0; $i < $csize; $i++) {
		$name = $DB->real_escape_string(basename($_FILES['file-1']['name'][$i]));
		$tmp_name = $_FILES['file-1']['tmp_name'][$i];
		$size = intval($_FILES['file-1']['size'][$i]);
        $nameUpload = date('Ymd_His') .'_'. $name;

        if (!$fileHandler->upload(file_get_contents($tmp_name), $nameUpload, '')) {
            $message->addError("File {$name} and beyond are not uploaded. Please upload them again.");
            redirect('RUSER_UPLOAD');
        }

        $query = "INSERT INTO `file_process` (`name`) VALUES ('{$nameUpload}')";
        $result = $DB->query($query);

		if($result == NULL) {
			$message->addError("Error inserting record of the file.");
            $message->addError("File {$name} and beyond are not uploaded. Please upload them again.");
            REDIRECT('RUSER_UPLOAD');
		}
	}
} else {
    $message->addError("No Files Given");
    redirect('RUSER_UPLOAD');
}
$socket = new Socket();
$socket->sendData("TEMP_PROCESS\n");
$socket->close();
$message->addInfo('Files Uploaded Successfully.');
$message->addInfo('It may take few minutes to reflect changes.');
redirect('RUSER_UPLOAD');
?>
 
 