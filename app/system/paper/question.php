<?php
var_dump($_POST);
if (!checkPost(['qpc', 'mq', 'sq', 'marks', 'question'])) {
	$message->addError("Please Fill All the Details");
	redirect('RUSER_ADD_QUESTIONS');
}
$QPCode = $DB->escape_string($_POST['qpc']);
$MQ = intval($_POST['mq']);
$SQ = ord($_POST['sq'][0])-ord('a');
$marks = intval($_POST['marks']);
$question = $DB->escape_string($_POST['question']);
$query = "INSERT INTO `questions` (`qp_code`, `main_question`, `sub_question`, `marks`, `question`) VALUES ('{$QPCode}', '{$MQ}', '{$SQ}', '{$marks}', '{$question}')";
$result = $DB->query($query);
if ($result != NULL) {
	$message->addInfo("Question Succuessfully Added");
} else {
	$message->addError("Question Already Exists for same QP Code.");
}
redirect('RUSER_ADD_QUESTIONS');
?>