<?php

function sendResponse($text)
{
	echo "<b id='www'>{$text}</b>";
	return;
}

if (!checkPost('x', 'y', 'q', 'text', 'page_no', 'paperId')) {
	sendResponse('-1');
} else {
	$x = $DB->escape_string($_POST['x']);
	$y = $DB->escape_string($_POST['y']);
	$marks = $DB->escape_string($_POST['text']);
	$page_no = $DB->escape_string($_POST['page_no']);
	$pid = $DB->escape_string($_POST['paperId']);
	$q = intval($_POST['q']);
	$query = "SELECT `id` FROM `marks` WHERE `page_no`='{$page_no}' AND `paperId`='{$pid}'";
	$result = $DB->query($query);
	$currentQ = $result->num_rows;
	if ($result != NULL && $result->num_rows >= $q) {
		sendResponse("-2");
	} else {
		$query = "INSERT INTO `marks` (x, y, marks, page_no, paperId) VALUES ('{$x}', '{$y}', '{$marks}', '{$page_no}', '{$pid}')";
		$DB->query($query);
		sendResponse($currentQ);
	}
}
?>