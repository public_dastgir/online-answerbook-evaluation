<?php
function numToLet($number) {
	$char = ord('a')+$number;
	return chr($char);
}

if (isset($_POST)) {
	$qp_code = $DB->escape_string($_POST['qp_code']);
	$paper_id = $DB->escape_string($_POST['paper_id']);
	$query = "SELECT `main_question`, `sub_question`, `marks` FROM `questions` WHERE `qp_code`='{$qp_code}'";
	$result = $DB->query($query);
	$questions = [];
	if ($result != NULL && $result->num_rows > 0) {
		while ($row = $result->fetch_assoc()) {
			if (!isset($questions[intval($row['main_question'])])) {
				$questions[intval($row['main_question'])] = [];
			}
			if (!isset($questions[intval($row['main_question'])][intval($row['sub_question'])])) {
				$q = "SELECT `id` FROM `paper_attempted` WHERE `question`='". (intval($row['main_question'])-1) ."' AND `sub_question`='". (intval($row['sub_question'])-1) ."' AND `paper_id`='{$paper_id}'";
				$res = $DB->query($q);
				$attempted = 0;
				if ($res != NULL && $res->num_rows > 0) {
					$attempted = 1;
				}
				$questions[intval($row['main_question'])][intval($row['sub_question'])] = [intval($row['marks']), $attempted];
			}
		}
	}
	//
	$query = "SELECT `question`, `sub_question`, `page_no` FROM `paper_attempted` LEFT JOIN `paper` ON `paper`.`qp_code`='{$qp_code}' AND `paper`.`paper_id`=`paper_attempted`.`paper_id`";
	$result = $DB->query($query);
	$page = [];
	if ($result != NULL && $result->num_rows > 0) {
		while ($row = $result->fetch_assoc()) {
			if (!isset($page[$row['page_no']])) {
				$page[$row['page_no']] = [];
			}
			$page[$row['page_no']][] = ($row['question']+1).''.numToLet($row['sub_question']);
		}
	}
	echo "<b id='wwww'>";
	echo json_encode($questions);
	echo "</b>";
	echo "<b id='www'>";
	echo json_encode($page);
	echo "</b>";
}
?>