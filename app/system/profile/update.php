<?php

// Get Password
$query = "SELECT `password` FROM `login` WHERE `id` = '$user->id'";

$result = $DB->query($query);
if (!checkPost(['password'])) {
	$message->addError("Please Enter Password Field.");
	redirect('RUSER_PROFILE_U');
}

// Get the Result
if ($result != NULL && $result->num_rows == 1) {
	$row = $result->fetch_assoc();
} else {
	if ($result == NULL) {
		$message->addError("DB Error: ". $DB->generateErrorMessage());
	} else {
		$message->addError("Cannot Check Database.");
	}
	redirect('RUSER_PROFILE_U');
}

// Verify Password
if (!password_verify($_POST['password'], $row['password'])) {
	$message->addError("Current Password Does not match.");
	redirect('RUSER_PROFILE_U');
}
$password = '';
$mob_no = '';
if (isset($_POST['npassword']) || isset($_POST['vpassword'])) {
	if (isset($_POST['npassword']) && isset($_POST['vpassword']) &&
		!empty($_POST['npassword']) && !empty($_POST['vpassword'])) {
		if ($_POST['npassword'] != $_POST['vpassword']) {
			$message->addError("Passwords does not match.");
			redirect('RUSER_PROFILE_U');
		} else {
			$password = $_POST['npassword'];
		}
	} else if (!empty($_POST['npassword']) && !empty($_POST['vpassword'])) {
		$message->addError("Please enter Password in both fields");
		redirect('RUSER_PROFILE_U');
	}
}
if (isset($_POST['mob_no'])) {
	if (empty($_POST['mob_no'])) {
		$message->addError("Mobile Number cannot be blank.");
		redirect('RUSER_PROFILE_U');
	} else {
		$mob_no = $_POST['mob_no'];
	}
}
if ($mob_no == '' && $password == '') {
	$message->addError("Nothing to Update...");
	redirect('RUSER_PROFILE_U');
}
$query = 'UPDATE `login` SET ';
if ($password != '') {
	$password_hash = password_hash($password, PASSWORD_BCRYPT);
	$password_hash = $DB->escape_string($password_hash);
	$query .= '`password`=\''. $password_hash .'\' ';
}
if ($mob_no != '') {
	if ($password != '') {
		$query .= 'AND ';
	}
	$mob_no = $DB->escape_string($mob_no);
	$query .= '`mobile_no`=\''. $mob_no .'\' ';
}
$query .= 'WHERE `id`=\''. $user->id .'\'';
if ($DB->query($query)) {
	$user->mobile_no = $mob_no;
	$message->addInfo("Profile Updated.");
	redirect('RUSER_PROFILE_U');
} else {
	$message->addError("Cannot Update Profile!!! Please Try again Later.");
	redirect('RUSER_PROFILE_U');
}
?>