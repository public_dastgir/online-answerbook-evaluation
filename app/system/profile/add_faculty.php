<?php
function genS($prefix, $length = 10)
{
	$valid = 'abcdefghijklmnopqrstuvwxyz0123456789';
	$string = substr(md5($prefix), 0, 6);
	$max = strlen($valid) - 1;
	for ($i = 0; $i < $length; $i++) {
		$string .= $valid[mt_rand(0, $max)];
	}
	return $string;
}

// Check if all requests have came.
$check = ['name', 'email', 'mob_no', 'sex', 'college', 'role'];
if (!checkPost($check)) {
	$message->addError("Not All Fields are flled.");
	redirect('RUSER_ADD_FACULTY');
}

// Check IP Lock
$locked = false;
if (isset($_POST['ip_locked']) && $_POST['ip_locked'] == 'on') {
	$locked = true;
}

// Check IPv6 or IPv4
if (isset($_POST['ip8']) && !empty($_POST['ip8'])) {
	$ip_max = 8;
} else {
	$ip_max = 4;
}

$ip = [];

// Generate User IP if Locked
if ($locked) {
	$ip = inet_pton($user->ip);
	$size = strlen($ip);
	if ($size == 16) {	// IPv6
		$ip = unpack("H32", $ip);
		$ip = $ip[1];
	} else if ($size == 4) {
		$ip = unpack("C4", $ip);
		// Shift IP from index 1 to 0.
		for ($i = 0; $i < 4; $i++) {
			$ip[$i] = $ip[$i+1];
		}
	} else {
		throw new Exception("Unknown IP Format");
	}
}

if ($user->ip_locked && $user->role != 8 && !$locked) {
	/**
	 * @todo Add Log of Attempt to hack?
	 */
	throw new Exception("Cannot Proceed");
}
$ip_string = '';
// Check if All IP Data received or not?
for ($i = $ip_max; $i > 0; $i--) {
	// IP is locked, check only last octat.
	if (($locked || ($user->ip_locked && $user->role != 8)) && $i < $ip_max) {
		if ($ip_max == 8) {
			$ip[$i] = str_pad($ip[$i], 4, "0", STR_PAD_LEFT); 
			$ip_string = $ip[$i].':'.$ip_string;
		} else {
			$ip_string = $ip[$i].'.'.$ip_string;
		}
		break;
	}
	if (!isset($_POST['ip'.$i]) || empty($_POST['ip'.$i])) {
		$message->addError("IP Data not received.");
		redirect('RUSER_ADD_FACULTY');
	}
	// Check Valid Format for IP and Generate IP String
	if ($ip_max == 8) {
		$ip[$i] = str_pad($_POST['ip'.$i], 4, "0", STR_PAD_LEFT); 
		if ($i == $ip_max) {
			$ip_string = $ip[$i];
		} else {
			$ip_string = $ip[$i].':'.$ip_string;
		}
		if (!ctype_xdigit($ip[$i])) {
			$message->addError("Invalid Format for IP {$i}.");
			redirect('RUSER_ADD_FACULTY');
		}
	} else {
		$ip[$i] = $_POST['ip'.$i];
		if ($i == $ip_max) {
			$ip_string = $ip[$i];
		} else {
			$ip_string = $ip[$i].'.'.$ip_string;
		}
		if ($ip[$i] < 0 || $ip[$i] > 255) {
			$message->addError("Invalid Format for IP {$i}.");
			redirect('RUSER_ADD_FACULTY');	
		}
	}
}

// Generate Username and Password
$username = genS($user->college_id);
$password = genS($ip_string);

// Validate Sex
if (!in_array($_POST['sex'], ['M', 'F', 'O'])) {
	$message->addError("Cannot Validate Sex Field");
	redirect('RUSER_ADD_FACULTY');
}
// Validate College
if ($user->role != 8 && intval($_POST['college']) != $user->college_id) {
	/** @todo Add Hack Attempt? */
	$message->addError("Invalid College Provided.");
	redirect('RUSER_ADD_FACULTY');
}
// Validate Role
if (intval($_POST['role']) >= $user->role) {
	/** @todo Add Hack Attempt? */
	$message->addError("Invalid Role Provided.");
	redirect('RUSER_ADD_FACULTY');	
}
$password_hash = password_hash($password, PASSWORD_BCRYPT);
$username = $DB->escape_string($username);
$password_hash = $DB->escape_string($password_hash);
$name = $DB->escape_string($_POST['name']);
$email = $DB->escape_string($_POST['email']);
$mob_no = $DB->escape_string($_POST['mob_no']);
$sex = $DB->escape_string($_POST['sex']);
$college = $DB->escape_string($_POST['college']);
$role = $DB->escape_string($_POST['role']);
$ip_string = $DB->escape_string($ip_string);
if ($locked) {
	$locked = '1';
} else {
	$locked = '0';
}



$query = "INSERT INTO `login` (`username`, `password`, `name`, `email`, `mobile_no`, `sex`, `role`, `college_id`, `verified`, `ip_locked`, `ip`, `unique_id`) VALUES
		('{$username}', '{$password_hash}', '{$name}', '{$email}', '{$mob_no}', '{$sex}', '{$role}', '{$college}', '2', '{$locked}', '{$ip_string}', '')";
if ($DB->query($query)) {
	$message->addInfo("Faculty Added Successfully.");
	$message->addInfo("Please Note these as these won't be displayed later");
	$message->addInfo("Username: {$username}");
	$message->addInfo("Password: {$password}");
	redirect('RUSER_ADD_FACULTY');
} else {
	unset($_SESSION['add_faculty_p']);
	$message->addError("Cannot Add Faculty!!! Please Try again Later(". $DB->generateErrorMessage() .").");
	redirect('RUSER_ADD_FACULTY');
}
?>