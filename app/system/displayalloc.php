<?php
	if (!checkPost(['allocate'])) {
		$message->addError("Cannot Allocate Paper");
		redirect('RUSER_ALLOC');
	}
	$subjectId = intval($_POST['allocate']);

	$query = "SELECT `paper_id`, `qp_code` FROM `paper` WHERE `status`='0' ORDER BY `paper_id` LIMIT 10";
	$result = $DB->query($query);
	$paper = [];
	if ($result != NULL && $result->num_rows > 0) {
		while ($row = $result->fetch_assoc()) {
			$paper[] = $row['paper_id'];
			$DB->query("UPDATE `paper` SET `status`='1' WHERE `paper_id`='{$row['paper_id']}'");
			$DB->query("INSERT INTO `allocates` (`pid`, `tid`) VALUES ('{$row['paper_id']}', '{$user->id}')");
		}
		$message->addInfo("{$result->num_rows} Paper Allocated");
	} else {
		$message->addError("No Paper Left to Allocate");
	}
	redirect('RUSER_ALLOC');

?>