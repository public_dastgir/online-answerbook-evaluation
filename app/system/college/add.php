<?php
if (checkPost(['name', 'reg_no']) == false) {
	$message->addError('Please Enter all the fields.');
	redirect('RUSER_ADD_COLLEGE');
}

$name = $DB->escape_string($_POST['name']);
$reg_no = $DB->escape_string($_POST['reg_no']);

$query = "INSERT INTO `college` (`name`, `reg_no`) VALUES ('{$name}', '{$reg_no}');";
if ($DB->query($query)) {
	$message->addInfo("College Added Successfully.");
} else {
	$message->addError("College Name or Registration Number already Exists.". $DB->generateErrorMessage());
}
redirect('RUSER_ADD_COLLEGE');
?>