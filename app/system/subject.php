<?php
//array(2) { ["sel"]=> array(2) { [0]=> string(1) "1" [1]=> string(1) "2" } ["tea"]=> string(1) "5" }
if (checkPost(['sel', 'tea']) == false) {
	$message->addError('Please Enter all the fields.');
	redirect('RUSER_SUBJECT');
}

$tea = $DB->escape_string($_POST['tea']);
$size = count($_POST['sel']);

$query = "DELETE FROM `teacher_sub` WHERE `teacher`='{$tea}'";
$DB->query($query);

for ($i = 0; $i < $size; $i++) {
	$sub = $_POST['sel'][$i];
	$query = "INSERT INTO `teacher_sub` (`subject`, `teacher`) VALUES ('{$sub}', '{$tea}');";
	$DB->query($query);
}

$message->addInfo("Teacher Assigned Successfully.");
redirect('RUSER_SUBJECT');
?>