<?php
/** Check if Username/Password is passed. */
if (!isset($_POST['username']) || empty($_POST['username']) ||
	!isset($_POST['password']) || empty($_POST['password']) ||
	!isset($_POST['signed_in']) || empty($_POST['signed_in'])
	) {
	$message->addError("Please fill all the fields.");
	redirect('RUSER_INDEX');
}

$username = $DB->real_escape_string($_POST['username']);
$pass = $DB->real_escape_string($_POST['password']);

// Select all Required Fields
$query = "SELECT `id`, `password`, `email`, `mobile_no`, `name`,  `sex`, `college_id`, `role`, `verified`, `unique_id`, `ip_locked`, `ip` FROM `login` WHERE `username` = '$username'";

$result = $DB->query($query);

// Get the Result
if ($result != NULL && $result->num_rows == 1) {
	$row = $result->fetch_assoc();
} else {
	if ($result == NULL) {
		$message->addError("DB Error: ". $DB->generateErrorMessage());
	} else {
		$message->addError("Invalid Username/Password Provided.");
	}
	redirect('RUSER_INDEX');
}

// Check if Verified
if (intval($row['verified']) == 0 || intval($row['verified']) > 2) {
	$message->addError("Your Account has not been yet verified by university admin.");
	$message->addError("Please contact University Admin for verification.");
	redirect('RUSER_INDEX');
}

// Verify the Password, if failed, Redirect back.
if (!password_verify($pass, $row['password'])) {
	$message->addError("Invalid Username/Password.");
	redirect('RUSER_INDEX');
}
if (!in_array($row['sex'], ['M', 'F', 'O'])) {
	$message->addError("Invalid Sex Specified.");
	redirect('RUSER_INDEX');
}
if ($row['college_id'] < 0) {
	$message->addError("College not Allocated.");
	redirect('RUSER_INDEX');
}
if ($row['role'] <= 2 && $row['college_id'] == 0) {
	$message->addError("Invalid College.");
	redirect('RUSER_INDEX');
}

// Check IP Lock
$user->ip = getClientIP();

if (intval($row['ip_locked'])) {
	if ($user->ip != $row['ip']) {
		$message->addError("The Account is IP-Locked,");
		$message->addError("Use the same PC or");
		$message->addError("Please Contact University Administrator in case IP has changed.");
		redirect('RUSER_INDEX');
	}
	$user->ip_locked = true;
} else {
	$user->ip_locked = false;
}

// Store Neccessary Information in user class.
$user->id = $row['id'];
$user->role = $row['role'];
$user->name = $row['name'];
$user->sex = $row['sex'];
$user->college_id = $row['college_id'];
$user->mobile_no = $row['mobile_no'];
$user->email = $row['email'];

// Get Subjects
$result2 = $DB->query("SELECT `t`.`subject` AS `id`, `s`.`subject` AS `name` FROM `teacher_sub` `t` LEFT JOIN `subject` `s` ON `s`.`id`=`t`.`subject` WHERE `t`.`teacher`='{$user->id}'");
if ($result2 != NULL && $result2->num_rows > 0) {
	while ($row2 = $result2->fetch_assoc()) {
		$user->subjects[] = $row2['id'];
		$user->subject_names[] = $row2['name'];
	}
}

// Check if password needs to be rehashed, if yes, rehash and save it.
if (password_needs_rehash($row['password'], PASSWORD_BCRYPT)) {
	$newHash = password_hash($pass, PASSWORD_BCRYPT);
	$DB->query("UPDATE `login` SET `password`='$newHash' WHERE `user`='$username'");
}

/** Session Id's are saved for use at only 1 pc */
if (!empty($row['unique_id'])) {
	// Create New Session ID
	session_regenerate_id();
	$session_id = session_id();
} else {
	$session_id = session_id();
}
// Set auth to true
$_SESSION['auth'] = true;
$_SESSION['user'] = serialize($user);


/** Update the unique_id */
$DB->query("UPDATE `login` SET `unique_id`='{$session_id}' WHERE `id`='{$row['id']}'");

redirect('RUSER_PROFILE');
?>
