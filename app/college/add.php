<div class="row">
	<form method="POST" action="<?php echo getRedirectUrl("RSYSTEM_ADD_COLLEGE"); ?>" class="col s12">
		<div class="row">
			<div class="input-field col s12">
				<input id="name" name="name" required type="text">
				<label for="name">College Name</label>
			</div>
			<div class="input-field col s12">
				<input id="reg_no" name="reg_no" required type="text">
				<label for="reg_no">Registration Number</label>
			</div>
		</div>
		<div class="row center">
			<button class="btn waves-effect waves-light" type="submit">Add College
				<i class="material-icons right">add</i>
			</button>
		</div>
	</form>
</div>
