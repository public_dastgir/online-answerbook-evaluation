<!DOCTYPE html>
<head>
    <title>MySQL file upload example</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="<?php echo getRedirectUrl('RCSS_UPLOAD'); ?>">
</head>
<body>
    <form action="<?php echo getRedirectUrl("RSYSTEM_UPLOAD_FILE"); ?>" method="post" enctype="multipart/form-data">
	<div class="input-field col s6">
		<input type="text" name="subject_name" id="subject_name"><br>
		<label for="subject_name">Subject Name</label><br><br>
	</div>
		<div class="box">
			<input type="file" style="display: none;"  name="file-1[]" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple />
			<label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
			<path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
		</div><br>
		<button class="btn waves-effect waves-light" type="submit" name="action">Upload File
			<i class="material-icons right">cloud</i>
		</button>
    </form>
    <p>
        <a href="<?php echo getRedirectUrl("RUSER_UPLOAD_VIEW"); ?>">See all files</a>
    </p>
	<script src="<?php echo getRedirectUrl("RJS_UPLOAD");?>"></script>
</body>
</html>