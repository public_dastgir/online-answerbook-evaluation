<?php



$sql = 'SELECT `id`, `name`, `mime`, `size`, `created` FROM `file`';
$result = $DB->query($sql);
 

if($result) {
    
    if($result->num_rows == 0) {
        echo '<p>There are no files in the database</p>';
    }
    else {
      
        echo '<table width="100%">
                <tr>
                    <td><b>Name</b></td>
                    <td><b>Mime</b></td>
                    <td><b>Size (bytes)</b></td>
                    <td><b>Created</b></td>
                    <td><b>&nbsp;</b></td>
                </tr>';
 
       
        while($row = $result->fetch_assoc()) {
            echo "
                <tr>
                    <td>{$row['name']}</td>
                    <td>{$row['mime']}</td>
                    <td>{$row['size']}</td>
                    <td>{$row['created']}</td>
                    
                </tr>";
        }
 
        
        echo '</table>';
    }
 
    
    $result->free();
}
else
{
    echo 'Error! SQL query failed:';
    echo "<pre>{$DB->error}</pre>";
}
?>