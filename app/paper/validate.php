<?php
	$result = $DB->query("SELECT `pid`, `paper`.`qp_code`, `paper`.`student_id` FROM `allocates` LEFT JOIN `paper` ON `paper`.`paper_id`=`allocates`.`pid` WHERE `tid`={$user->id}");
	if ($result == NULL || $result->num_rows == 0) {
		$message->addError("Please Allocate the Papers before Validation.");
		redirect('RUSER_INDEX');
	}
	$row = $result->fetch_assoc();
	$paperId = $row['pid'];
	$fileHandler = new FileHandler(FILETYPE_PAPER);
	$fileHandler->deepDIR([$row['qp_code'], $row['student_id']]);
	$count = $fileHandler->count();
?>
<div class="row grey">
	<!-- Paper Display -->
	<div id="p1" class="col s8 offset-s1 grey"  style="overflow-y: scroll;"></div>
	<!-- Question Numbers -->
	<div id="r1" class="col s1 blue" style="overflow-y: scroll;">
		
	</div>
	<div id="r3x" class="col s2 blue" style="overflow-y: scroll;">
		<i id="r3a"></i>
		<i id="r3">No Marks would be given upon click</i>
		<!-- Left Navigation Bar(Log/Delete) -->
		<!--
		<div>
			<a class="waves-effect waves-light btn-large" id="undo"><i class="material-icons left">undo</i>Undo</a><br/><br/>
			 
			<a class='dropdown-button btn' href='#' data-activates='dropdownD'>Delete</a>

			
			<ul id='dropdownD' class='dropdown-content'>
				<li><a href="#!">one</a></li>
				<li><a href="#!">two</a></li>
				<li class="divider"></li>
				<li><a href="#!">three</a></li>
				<li><a href="#!"><i class="material-icons">view_module</i>four</a></li>
				<li><a href="#!"><i class="material-icons">cloud</i>five</a></li>
			</ul>
		</div>
		-->
	</div>
	<!-- Question/Answer Display -->
	<div id="r2" class="col s3 light-blue" style="height:200px; overflow-y: scroll;"></div>
</div>
<!-- JS -->
<script type="text/javascript" src="<?php echo getRedirectUrl('RJS_PAPER_V'); ?>"></script>
<?php

?>
<script type="text/javascript">
var last_index_marks;
var paper_id = <?php echo $paperId; ?>;
var qp_code = '<?php echo $row['qp_code']; ?>';
// Canvas Loading
// Initialize Canvases
var max_paper = <?php echo $fileHandler->count(); ?>;
var canvasBody = '';
var canvas = [];
var ctx = [];
var p1 = document.getElementById('p1');
var page_no = 1;
var height_of_canvas = 0;
var page_times = 0;

var page_q = [];

var page_obj;

function ChangeQuestion(scrolled, height)
{
	height = height+380;
	var pg_no = Math.floor(scrolled/height);
	console.log("Current Page Number: "+ pg_no);
	
	callAjaxHelper(pg_no)
}
ChangeQuestion(0,1)

function callAjaxHelper(page_no) {
	var url = "<?php echo getRedirectUrl('RSYSTEM_GET_QUESTION'); ?>";
	var data = { 
		page_no: page_no,
		paper_id: paper_id,
		qp_code: qp_code
	};
	callAjax(url, data, ajaxComplete);
}

function ajaxComplete(o) {
	var x = $(o).find('#wwww').html();
	var y = $(o).find('#www').html();
	if (x != '') {
		var que_div = document.getElementById('r2');
		que_div.innerHTML = decodeURI(x)
		var que_div = document.getElementById('r3a');
		que_div.innerHTML = 'You can only enter marks in this page for '+ y +' times';
		page_times = y;

	}
}

function callAjax(postUrl, postData, funcToCall) {
	$.ajax({
		type: "POST",
		url: postUrl,
		data: postData
	}).done(function(o) {
		funcToCall(o);
	});
}

function isNumeric(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}

// When Canvas is clicked.
function on_canvas_click(ev, i) {
	// Get x and y co-ordinates from canvas.
	var rect = canvas[i].getBoundingClientRect();
	console.log(rect);
	var x = ev.clientX - rect.left;
	var y = ev.clientY - rect.top;
	// Get value/color from input box.
	var color = "red";
	var value = lastNumber;

	// No Number = 0.
	if (!isNumeric(value))
		value = 0;
	// No Color means read.
	if (color == "")
		color = "red";
	// Fill the Canvas
	if (lastNumber != -1) {
		last_index_marks = i
		sendPost(x, y, value, i+2, paper_id);
	}
}

/**
 * Sends POST Request and Saves the Marks.
 * @method sendPost
 */
function sendPost(xc, yc, text, page_no, paperId) {
	// Send POST via AJAX.
	$.ajax({
		type: "POST",
		url: "<?php echo getRedirectURL('RSYSTEM_ADD_MARKS'); ?>",
		data: { 
			text: text,
			x: xc,
			y: yc,
			page_no: page_no,
			paperId: paperId,
			q: page_times,
		},
		beforeSend: function() {
			// Status before send.
			//save.innerHTML = "Saving...";
		},
		error: function() {
			// OnError Status.
			//save.innerHTML = "Unable to save.";
		}
	}).done(function(o) {
		//save.innerHTML = "Saved";
		var x = $(o).find('#www').html();
		console.log(x);
		switch(x) {
			default:
				QuestionNumber = parseInt(x);
				var id_of_div = page_q[page_no][QuestionNumber];
				var doc = document.getElementById(id_of_div);
				var doc2 = document.getElementById(id_of_div+"X");
				console.log(id_of_div+"X")
				console.log("Doc", doc, doc2);
				if (text > parseInt(doc2.innerHTML)) {
					return;
				}
				doc.innerHTML = text;
				// Check for Completion
				completed = true;
				// Canvas
				var ctxx = canvas[page_no-2].getContext('2d');
				ctxx.font = "30px Arial";
				ctxx.fillText(text, xc, yc);
				//
				console.log(page_obj);
				for (var mq in page_obj) {
					for (var sq in page_obj[mq]) {
						sq = String.fromCharCode(96+parseInt(sq));
						var doc = document.getElementById(mq+''+sq);
						if (doc.innerHTML == '_') {
							completed = false;
							break;
						}
					}
				}
				console.log("Completed Tag: "+ completed);
				window.location = "<?php echo getRedirectURL("RUSER_INDEX"); ?>";
				break;
			case "-1":
			case "-2":
				console.log("Some Error has Occurred");
				break;
		}
		return true;
	});
	return true;
}


function completeAjax() {
	var url = "<?php echo getRedirectUrl('RSYSTEM_COMPLETE_PAPER'); ?>";
	var data = { 
		qp_code: qp_code,
		paper_id: paper_id
	};
	callAjax(url, data, ajaxComplete3);
}

function ajaxComplete3() {

}



function callAjaxHelperAllQuestions() {
	var url = "<?php echo getRedirectUrl('RSYSTEM_GET_QUESTIONS'); ?>";
	var data = { 
		qp_code: qp_code,
		paper_id: paper_id
	};
	callAjax(url, data, ajaxComplete2);
}


function ajaxComplete2(o) {
	var x = $(o).find('#wwww').html();
	if (x != '') {
		var que_div = document.getElementById('r1');
		que_div.innerHTML = x;
		obj = JSON.parse(x);
		i = ''
		//page_obj = obj;
		console.log(obj);
		page_obj = obj;
		for (var mq in obj) {
			for (var sq in obj[mq]) {
				marks = obj[mq][sq][0]
				attempted = parseInt(obj[mq][sq][1]);
				sq = String.fromCharCode(96+parseInt(sq));
				if (attempted == 1) {
					i += 'Q'+ mq +"."+ sq +": <div style='display: inline;' id='"+ mq +""+ sq +"'>_</div> / <div style='display: inline;' id='"+ mq +""+ sq +"X'>"+ marks +"</div><br/>";
				} else {
					i += 'Q'+ mq +"."+ sq +": <div style='display: inline;' id='"+ mq +""+ sq +"'>N</div> / <div style='display: inline;' id='"+ mq +""+ sq +"X'>"+ marks +"</div><br/>";
				}
			}
		}
		que_div.innerHTML = i
	}
	/// www
	var x = $(o).find('#www').html();
	if (x != '') {
		page_q = JSON.parse(x);
	}
}
callAjaxHelperAllQuestions();



<?php
	for ($i = 0; $i < $fileHandler->count(); $i++) {
		echo 'canvasBody += \'<canvas id="myCanvas'. $i .'" style="border:1px solid #000000;" ></canvas>\';'."\n";
	}
	echo 'p1.innerHTML = canvasBody;'."\n";
	echo 'var img = [];'."\n";
	echo 'var height = 0;'."\n";
	for ($i = 0; $i < $fileHandler->count(); $i++) {
		echo 'img['. $i .'] = new Image;'."\n";
		echo 'img['. $i .'].src = "data:image/png;base64,'. base64_encode($fileHandler->get($i+2)) .'";'."\n";
	}
	for ($i = 0; $i < $fileHandler->count(); $i++) {
		//echo 'clb['. $i .'] = new CanvasLogBook();'. "\n";
		echo 'canvas['. $i .'] = document.getElementById(\'myCanvas'.$i.'\');'."\n";
		echo 'ctx['. $i .'] = canvas['. $i .'].getContext(\'2d\');'."\n";
		//echo 'clb['. $i .'].page_no = '. ($i+1) .';'."\n";
		//echo 'clb['. $i .'].ctx = ctx['. $i .'];'."\n";
		echo "
			img[". $i ."].onload = function() {
			height = innerHeight - 100;
			p1.style = \"height: \"+ height +\"px; overflow-y: scroll;\";
			document.getElementById('r1').style = \"height: \"+ (height - 200) +\"px; display:block;\";
			document.getElementById('r3').style = \"height: \"+ (height - 265) +\"px; display:block;\";
			console.log(height, innerHeight);
		    	//canvas[". $i ."].height = height;
		    	canvas[". $i ."].height = img[". $i ."].height/4;
		    	canvas[". $i ."].width = img[". $i ."].width/4;
		    	// clb[". $i ."].baseImg = img[". $i ."];
		    	canvas[". $i ."].addEventListener('click', function(ev) { on_canvas_click(ev, ". $i ."); });
		    	ctx[". $i ."].drawImage(img[". $i ."], 0, 0, img[". $i ."].width/4, img[". $i ."].height/4);
		    	height_of_canvas = canvas[". $i ."].height/2;
		}
		";
	}
?>

</script>
<script type="text/javascript" src="<?php echo getRedirectUrl('RJS_PAPER_V'); ?>"></script>
