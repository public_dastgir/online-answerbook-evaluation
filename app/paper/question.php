<div class="row">
	<form method="POST" action="<?php echo getRedirectUrl("RSYSTEM_ADD_QUESTIONS"); ?>" class="col s12">
		<div class="row">
			<div class="input-field col s2">
				<input required placeholder="QP Code" name="qpc" id="qpc" type="text" class="validate">
				<label for="qpc">QP Code</label>
			</div>

			<div class="input-field col s1 offset-s3">
				<input required placeholder="1" name="mq" id="mq" type="number" class="validate" min='1' max='9'>
				<label for="mq">Main Question</label>
			</div>

			<div class="input-field col s1">
				<input required placeholder="a" name="sq" id="sq" type="text" class="validate">
				<label for="sq">Sub Question</label>
			</div>

			<div class="input-field col s1 offset-s3">
				<input required placeholder="1" name="marks" id="marks" type="number" class="validate" min='1'>
				<label for="marks">Marks</label>
			</div>

			<div class="input-field col s12">
				<textarea id="question" name="question" class="materialize-textarea"></textarea>
          		<label for="question">Enter the Question</label>
			</div>
		</div>
		<button type="submit" class="btn-floating right btn-large waves-effect waves-light green"><i class="material-icons">add</i></a>
	</form>
</div>
