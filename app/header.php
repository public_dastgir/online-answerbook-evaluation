<html>
<head>
	<!-- <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
	<title><?php echo $MAIN_title; ?></title>
	<link type="text/css" rel="stylesheet" href="<?php echo getRedirectUrl('RCSS_MATERIAL_ICONS'); ?>"  media="screen,projection"/>
	<link type="text/css" rel="stylesheet" href="<?php echo getRedirectUrl('RCSS_MATERIALIZE_MIN'); ?>"  media="screen,projection"/>
	<script type="text/javascript" src="<?php echo getRedirectUrl('RJS_JQUERY'); ?>"></script>
	<link rel="stylesheet" href="<?php echo getRedirectUrl('RCSS_MORRIS'); ?>">
	<script type="text/javascript" src="<?php echo getRedirectUrl('RJS_RAPHEAL'); ?>"></script>
	<script type="text/javascript" src="<?php echo getRedirectUrl('RJS_MORRIS'); ?>"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
<?php
	$nav = new Navigation();
	$nav2 = new Navigation(true);
	list($dropdown, $navigation) = $nav->getNavBar();
	list($dropdown2, $navigation2) = $nav2->getNavBar();
	echo $dropdown;
	echo $dropdown2;
?>
<nav class="<?php if($user->role==1) {echo 'light-blue';} 
				  else if($user->role==2){echo 'red';}
					else if($user->role==4){echo 'orange';}
						else if($user->role==8){echo 'green';}	?>" lighten-1" role="navigation">
	<div class="nav-wrapper container">
		<a id="logo-container" href="#" class="brand-logo">OPES</a>
		<ul class="right hide-on-med-and-down">
<?php
		echo $navigation;
?>
		</ul>

	  	<ul id="nav-mobile" class="side-nav">
<?php
		echo $navigation2;
?>
	  	</ul>
		<a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
	</div>
  </nav>
<br/>
<?php
	if ($MAIN_role == 'validate_paper') {
		echo '<div>';
	} else {
		echo '<div class="container">';
	}
	// Display Error
	$errors = $message->getError();
	if (count($errors)) {
		echo '<div class="card-panel red" style="width:75%;">';
		echo '<b>Error:</b><br/>';
		foreach ($errors as $error) {
			echo '<span class="text-darken-2">'. $error .'</span><br/>';
		}
		echo '</div>';
		$message->clearError();
	}
	// Info
	$infos = $message->getInfo();
	if (count($infos)) {
		echo '<div class="card-panel" style="width:75%;">';
		echo '<i>Information:</i><br/>';
		foreach ($infos as $info) {
			echo '<span class="blue-text text-darken-2">'. $info .'</span><br/>';
		}
		echo '</div>';
		$message->clearInfo();
	}
	// Debug Message Display
	$debugs = $message->getDebug();
	if (count($debugs)) {
		echo '<div class="card-panel green" style="width:75%;">';
		echo '<i class="white-text">Debug:</i><br/>';
		foreach ($debugs as $debug) {
			echo '<span class="white-text text-darken-2">'. $debug .'</span><br/>';
		}
		echo '</div>';
		$message->clearDebug();
	}
?>